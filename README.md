# laravel-flarum-forum

#### 介绍
仅基于`flarum`样式

#### 软件架构
软件架构说明
- Laravel version 10 or later
- PHP version 8.1 or later
	* fileinfo
	* curl
	* pdo_mysql
	* sodium
- Node.js（16+） => v16.13.1
	+ livewire => `Vite`依赖版本限制

#### 安装教程

1. composer install
2. php artisan migrate
3. 数据填充：php artisan module:seed
   1. 会员填充：php artisan module:seed --class=UserTableSeeder Forum
4. 创建`Storage`目录软链接 `php artisan storage:link`
    1. docker环境使用`php artisan storage:link --relative`
5. 发布编辑器`pandao/editor.md`相关文件：`php artisan vendor:publish --provider="Cnpscy\LaravelEditormd\EditorMdProvider"`

#### 页面路径
1. 会员主页 /user/{id}

#### 使用说明

1. 多模块scss、js通过根目录的`vite.config.js`调度同时生效的配置
   1. `vite.config.js`内的`modulesPath`模块`目录文件夹`与PHP`config => modules.paths.modules(目录文件夹)`内的`目录文件夹`保持一致！
   2. 多模块化内对应的`vite.config.js`参考`主模块 => Forum`启用配置`paths`，并且需`$STUDLY_NAME$`变更为模块目录名称！
      1. `npm run dev || npm run build` 模式下，样式皆会同步生效！
      2. `$STUDLY_NAME$`不变更的话，`npm run build`无法生成对应模块的样式`Js、css`。
   3. 多模块样式在视图层可独立调用或自动调用皆可。
2. 关于在多模块内部使用`livewire`
   1. 如果是直接调用默认路径的组件：`<livewire:auth.login />`
   2. 如果需读取的是模块化的`livewire`组件，有两种方式
      1. 注册组件服务，在对应模板的`ServiceProvider::boot`
         1. 注册`Livewire::component('component-name', \App\Http\Livewire\ComponentName::class);`
         2. 调用`<livewire:component-name />`
      2. 目前`livewire`尚未兼容`多模块组件`，且定义了目录与命名空间，所以只能使用指定`class`类，通过`Livewire::mount`调用即可：
         1. `{!! \Livewire\Livewire::mount(App\Modules\Forum\App\Livewire\Dynamic\Lists::class); !!}`
   3. `livewire`组件内容层，根层必须是一个`div`包起的内容，哪怕是`@if + @else`仅返回一个`div`，也许把它们仅包含在一个`div`内部。 
3. 一键加载多模块的css与js
   1. `@vite(\Nwidart\Modules\Module::getAssets())`，依赖于``` 多模块scss、js通过根目录的`vite.config.js`调度同时生效的配置 ```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 使用扩展包
- Passport OAuth 认证 `composer require laravel/passport`
- 模块化组件 `nwidart/laravel-modules:^10.0.6`
- Livewire Calendar 日历模块 `composer require asantibanez/livewire-calendar`
    + 原版本不支持`"livewire/livewire": "^3.4"`，使用扩展包`composer require omnia-digital/livewire-calendar:3.0.1`
- Livewire Charts `composer require asantibanez/livewire-charts`


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
