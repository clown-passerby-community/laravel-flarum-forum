<x-app-layout>
    <div class="IndexPage">
        <header class="Hero WelcomeHero">
            <div class="container">
                <button class="Hero-close Button Button--icon Button--link hasIcon" type="button" aria-label="隐藏欢迎横幅内容">
                    <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                    <span class="Button-label"></span>
                </button>
                <div class="containerNarrow">
                    <h1 class="Hero-title">Welcome to 小丑路人的flarum社区</h1>
                    <div class="Hero-subtitle">Enjoy your new forum! Hop over to discuss.flarum.org if you have any questions, or to join our community!</div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="sideNavContainer">
                <nav class="IndexPage-nav sideNav">
                    <ul>
                        <li class="item-newDiscussion App-primaryControl">
                            <button class="Button Button--primary IndexPage-newDiscussion hasIcon" type="button" itemclassname="App-primaryControl">
                                <i aria-hidden="true" class="icon fas fa-edit Button-icon"></i>
                                <span class="Button-label">发布主题</span>
                            </button>
                        </li>
                        <li class="item-nav">
                            <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount8">
                                <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="导航栏下拉菜单开关" data-toggle="dropdown">
                                    <span class="Button-label">全部主题</span>
                                    <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu ">
                                    <li class="item-allDiscussions active">
                                        <a class="hasIcon" href="/" active="true">
                                            <i aria-hidden="true" class="icon far fa-comments Button-icon"></i>
                                            <span class="Button-label">全部主题</span>
                                        </a>
                                    </li>
                                    <li class="item-fof-user-directory">
                                        <a class="hasIcon" href="/users" active="false">
                                            <i aria-hidden="true" class="icon far fa-address-book Button-icon"></i>
                                            <span class="Button-label">会员名录</span>
                                        </a>
                                    </li>
                                    <li class="item-privateDiscussions">
                                        <a class="hasIcon" href="/private" active="false">
                                            <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                            <span class="Button-label">私密主题</span>
                                        </a>
                                    </li>
                                    <li class="item-following">
                                        <a class="hasIcon" href="/following" active="false">
                                            <i aria-hidden="true" class="icon fas fa-star Button-icon"></i>
                                            <span class="Button-label">我的关注</span>
                                        </a>
                                    </li>
                                    <li class="item-tags">
                                        <a class="hasIcon" href="/tags" active="false">
                                            <i aria-hidden="true" class="icon fas fa-th-large Button-icon"></i>
                                            <span class="Button-label">标签</span>
                                        </a>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-tag1">
                                        <a class="TagLinkButton hasIcon" href="/t/default" style="--color: #888;">
                                            <span class="Button-icon icon TagIcon" style="--color: #888;"></span>
                                            <span class="Button-label">默认</span>
                                        </a>
                                    </li>
                                    <li class="item-tag2">
                                        <a class="TagLinkButton hasIcon" href="/t/Technical-articles" title="1" style="">
                                            <span class="Button-icon icon TagIcon" style=""></span>
                                            <span class="Button-label">技术文章</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="item-forumStatisticsWidget">
                            <div class="ForumStatistics containerNarrow">
                                <div class="row">
                                    <h2>
                                        <i class="fas fa-chart-bar"></i> 论坛统计
                                    </h2>
                                    <div>
                                        <ul id="ForumStatisticsList">
                                            <li>主题数：24</li>
                                            <li>回复数：31</li>
                                            <li>会员数：3</li>
                                            <li>最近注册： <a href="/u/liuneng">
                                                    <strong>
                                                        <span class="username">liuneng</span>
                                                    </strong>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="item-onlineUsers">
                            <div class="OnlineUsers">
                                <div class="OnlineUsers-title">在线会员</div>
                                <div class="OnlineUsers-list">
                                    <div class="OnlineUsers-item">
                                        <a href="/u/admin" title="小丑路人">
                                            <span class="Avatar OnlineUsers-avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                            <span class="username">小丑路人</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
                <div class="IndexPage-results sideNavOffset">
                    <div class="IndexPage-toolbar">
                        <ul class="IndexPage-toolbar-view">
                            <li class="item-sort">
                <span class="Select">
                    <select class="Select-input FormControl">
                        <option value="default">默认</option>
                        <option value="username_az">用户名（a-z）</option>
                        <option value="username_za">用户名（z-a）</option>
                        <option value="newest">新用户</option>
                        <option value="oldest">老用户</option>
                        <option value="most_discussions">主题最多</option>
                        <option value="least_discussions">主题最少</option>
                        <option value="most_best_answers">Most best answers</option>
                        <option value="least_best_answers">Least best answers</option>
                    </select>
                    <i aria-hidden="true" class="icon fas fa-sort Select-caret"></i>
                </span>
                            </li>
                            <li class="item-filterGroups">
                                <div class="ButtonGroup Dropdown dropdown GroupFilterDropdown itemCount4">
                                    <button class="Dropdown-toggle FormControl" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown" aria-expanded="false">
                                        <span class="Button-label">筛选用户组</span>
                                        <i aria-hidden="true" class="icon fas fa-filter Button-caret"></i>
                                    </button>
                                    <ul class="Dropdown-menu dropdown-menu">
                                        <li class="item-suspend">
                                            <button class="GroupFilterButton hasIcon" type="button">
                                                <i aria-hidden="true" class="icon fas fa-ban Button-icon"></i>
                                                <span class="Button-label">封禁中</span>
                                            </button>
                                        </li>
                                        <li class="Dropdown-separator"></li>
                                        <li class="item-管理组">
                                            <button class="GroupFilterButton hasIcon" type="button">
                                                <i aria-hidden="true" class="icon fas fa-wrench Button-icon"></i>
                                                <span class="Button-label">管理组</span>
                                            </button>
                                        </li>
                                        <li class="item-版主组">
                                            <button class="GroupFilterButton hasIcon" type="button">
                                                <i aria-hidden="true" class="icon fas fa-bolt Button-icon"></i>
                                                <span class="Button-label">版主组</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="item-search">
                                <div class="Form-group Usersearchbox">
                                    <label class="UserDirectorySearchInput FormControl ">
                                        <span class="UserDirectorySearchInput-selected"></span>
                                        <input class="FormControl" placeholder="搜索用户">
                                    </label>
                                </div>
                            </li>
                        </ul>
                        <ul class="IndexPage-toolbar-action">
                            <li class="item-refresh">
                                <button class="Button Button--icon hasIcon" type="button" aria-label="刷新">
                                    <i aria-hidden="true" class="icon fas fa-sync Button-icon"></i>
                                    <span class="Button-label"></span>
                                </button>
                            </li>
                        </ul>
                    </div>

                    <!-- 动态列表 -->
                    <livewire:user.users />
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
