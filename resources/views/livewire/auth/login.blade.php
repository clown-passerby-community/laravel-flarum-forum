<div>
    <livewire:components.modal id="login-modal" content='
    <div class="Modal-header">
        <h3 class="App-titleControl App-titleControl--text">登录</h3>
    </div>
    <div class="Modal-body">
        <div class="LogInButtons">
            <button class="Button LogInButton--WeChatAuth LogInButton hasIcon" type="button" path="/auth/wechat">
                <i aria-hidden="true" class="icon fab fa-weixin Button-icon"></i>
                <span class="Button-label">微信账号登录</span>
            </button>
            <button class="Button LogInButton--QQ LogInButton hasIcon" type="button" path="/api/auth/qq">
                <i aria-hidden="true" class="icon fab fa-qq Button-icon"></i>
                <span class="Button-label">QQ 账号登录</span>
            </button>
            <div class="LogInButtonContainer LogInButtonContainer--discord">
                <button class="Button FoFLogInButton LogInButton--discord LogInButton hasIcon" type="button" path="/auth/discord">
                    <i aria-hidden="true" class="icon fab fa-discord Button-icon"></i>
                    <span class="Button-label">Discord 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--facebook">
                <button class="Button FoFLogInButton LogInButton--facebook LogInButton hasIcon" type="button" path="/auth/facebook">
                    <i aria-hidden="true" class="icon fab fa-facebook Button-icon"></i>
                    <span class="Button-label">Facebook 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--github">
                <button class="Button FoFLogInButton LogInButton--github LogInButton hasIcon" type="button" path="/auth/github">
                    <i aria-hidden="true" class="icon fab fa-github Button-icon"></i>
                    <span class="Button-label">GitHub 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--gitlab">
                <button class="Button FoFLogInButton LogInButton--gitlab LogInButton hasIcon" type="button" path="/auth/gitlab">
                    <i aria-hidden="true" class="icon fab fa-gitlab Button-icon"></i>
                    <span class="Button-label">GitLab 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--twitter">
                <button class="Button FoFLogInButton LogInButton--twitter LogInButton hasIcon" type="button" path="/auth/twitter">
                    <i aria-hidden="true" class="icon fab fa-twitter Button-icon"></i>
                    <span class="Button-label">Twitter 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--linkedin">
                <button class="Button FoFLogInButton LogInButton--linkedin LogInButton hasIcon" type="button" path="/auth/linkedin">
                    <i aria-hidden="true" class="icon fab fa-linkedin Button-icon"></i>
                    <span class="Button-label">领英 账号登录</span>
                </button>
            </div>
            <div class="LogInButtonContainer LogInButtonContainer--google">
                <button class="Button FoFLogInButton LogInButton--google LogInButton hasIcon" type="button" path="/auth/google">
                    <i aria-hidden="true" class="icon fab fa-google Button-icon"></i>
                    <span class="Button-label">Google 账号登录</span>
                </button>
            </div>
        </div>
        <div class="Form Form--centered">
            <div class="Form-group">
                <input class="FormControl" name="identification" type="text" placeholder="用户名或邮箱" aria-label="用户名或邮箱">
            </div>
            <div class="Form-group">
                <input class="FormControl" name="password" type="password" autocomplete="current-password" placeholder="密码" aria-label="密码">
            </div>
            <div class="Form-group">
                <div>
                    <label class="checkbox">
                        <input type="checkbox">记住我的登录状态</label>
                </div>
            </div>
            <div class="Form-group">
                <button class="Button Button--primary Button--block" type="submit">
                    <span class="Button-label">登录</span>
                </button>
            </div>
        </div>
    </div>
    <div class="Modal-footer">
        <p class="LogInModal-forgotPassword">
            <a onclick={this.forgotPassword.bind(this)}>忘记密码？</a>
        </p>
        <p class="LogInModal-signUp">还没有帐户？ <a onclick="">立即注册</a></p>
    </div>
' />

    <script>
        function showLoginModal()
        {
            console.log(11111)
            $('#modal').html($('#login-modal').html());
        }
    </script>

</div>
