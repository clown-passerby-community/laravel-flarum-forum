<div>
    {{-- If your happiness depends on money, you will never be happy with yourself. --}}
    <form wire:submit="save">
        <input type="text" wire:model="form.title">
        <div>
            @error('form.title') <span class="error">{{ $message }}</span> @enderror
        </div>

        <textarea wire:model="form.content"></textarea>
        <div>
            @error('form.content') <span class="error">{{ $message }}</span> @enderror
        </div>

        <x-input-text name="title" wire:model="form.title" />

        <x-input-text name="content" wire:model="form.content" />

        <button type="submit">Save</button>

        <span wire:loading>Saving...</span>
    </form>
</div>
