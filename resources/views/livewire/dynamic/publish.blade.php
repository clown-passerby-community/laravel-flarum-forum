<div class="App-composer">
    <div class="container">
        <div id="composer">
            <div class="Composer normal visible" style="bottom: 0px; height: 300px;">
                <div class="Composer-handle" style="cursor: row-resize;"></div>
                <ul class="Composer-controls">
                    <li class="item-minimize App-backControl">
                        <button class="Button Button--icon Button--link hasIcon" type="button" aria-label="最小化" itemclassname="App-backControl">
                            <i aria-hidden="true" class="icon fas fa-minus minimize Button-icon"></i>
                            <span class="Button-label"></span>
                        </button>
                    </li>
                    <li class="item-fullScreen">
                        <button class="Button Button--icon Button--link hasIcon" type="button" aria-label="全屏">
                            <i aria-hidden="true" class="icon fas fa-expand Button-icon"></i>
                            <span class="Button-label"></span>
                        </button>
                    </li>
                    <li class="item-close">
                        <button class="Button Button--icon Button--link hasIcon" type="button" aria-label="关闭">
                            <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                            <span class="Button-label"></span>
                        </button>
                    </li>
                </ul>
                <div class="Composer-content">
                    <div class="ComposerBody ComposerBody--discussion">
                        <span class="Avatar ComposerBody-avatar" loading="lazy" style="--avatar-bg: #a0e5cf;">A</span>
                        <div class="ComposerBody-content">
                            <ul class="ComposerBody-header">
                                <li class="item-title">
                                    <h3>发布主题</h3>
                                </li>
                                <li class="item-tags">
                                    <a class="DiscussionComposer-changeTags">
                                        <span class="TagLabel untagged">添加标签</span>
                                    </a>
                                </li>
                                <li class="item-discussionTitle">
                                    <h3>
                                        <input class="FormControl" placeholder="标题">
                                    </h3>
                                </li>
                            </ul>
                            <div class="ComposerBody-editor">
                                <div class="TextEditor">
                                    <div class="TextEditor-editorContainer">
                                        <div class="ComposerBody-mentionsWrapper">
                                            <div class="ComposerBody-emojiWrapper">
                                                <textarea class="FormControl Composer-flexible TextEditor-editor" placeholder="说点什么吧..." style="height: 168.25px;"></textarea>
                                                <div class="ComposerBody-emojiDropdownContainer"></div>
                                            </div>
                                            <div class="ComposerBody-mentionsDropdownContainer"></div>
                                        </div>
                                    </div>
                                    <ul class="TextEditor-controls Composer-footer">
                                        <li class="item-submit App-primaryControl">
                                            <button class="Button Button--primary hasIcon" type="button" itemclassname="App-primaryControl">
                                                <i aria-hidden="true" class="icon fas fa-paper-plane Button-icon"></i>
                                                <span class="Button-label">发布</span>
                                            </button>
                                        </li>
                                        <li class="TextEditor-toolbar">
                                            <div class="MarkdownToolbar">
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="标题文本" data-original-title="标题文本">
                                                    <i aria-hidden="true" class="icon fas fa-heading"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="加粗文本 <ctrl-b>" data-original-title="加粗文本 <ctrl-b>">
                                                    <i aria-hidden="true" class="icon fas fa-bold"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="斜体 <ctrl-i>" data-original-title="斜体 <ctrl-i>">
                                                    <i aria-hidden="true" class="icon fas fa-italic"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="删除线" data-original-title="删除线">
                                                    <i aria-hidden="true" class="icon fas fa-strikethrough"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="引用" data-original-title="引用">
                                                    <i aria-hidden="true" class="icon fas fa-quote-left"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="黑幕" data-original-title="黑幕">
                                                    <i aria-hidden="true" class="icon fas fa-exclamation-triangle"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="行内代码" data-original-title="行内代码">
                                                    <i aria-hidden="true" class="icon fas fa-code"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="链接" data-original-title="链接">
                                                    <i aria-hidden="true" class="icon fas fa-link"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="图片" data-original-title="图片">
                                                    <i aria-hidden="true" class="icon fas fa-image"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="无序列表" data-original-title="无序列表">
                                                    <i aria-hidden="true" class="icon fas fa-list-ul"></i>
                                                </button>
                                                <button class="Button Button--icon Button--link" type="button" title="" aria-label="有序列表" data-original-title="有序列表">
                                                    <i aria-hidden="true" class="icon fas fa-list-ol"></i>
                                                </button>
                                            </div>
                                            <button class="Button Button--icon Button--link hasIcon" type="button" title="" aria-label="提及用户、组或楼层" data-original-title="提及用户、组或楼层">
                                                <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                                                <span class="Button-label">提及用户、组或楼层</span>
                                            </button>
                                            <button class="Button Button--icon Button--link hasIcon" type="button" title="" aria-label="表情" data-original-title="表情">
                                                <i aria-hidden="true" class="icon far fa-smile Button-icon"></i>
                                                <span class="Button-label">表情</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div aria-label="正在加载…" role="status" data-size="large" class="LoadingIndicator-container LoadingIndicator-container--large ComposerBody-loading">
                            <div aria-hidden="true" class="LoadingIndicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
