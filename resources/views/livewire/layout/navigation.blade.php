<nav>
    <!-- Primary Navigation Menu -->
    <div id="app-navigation" class="App-navigation">
        <div class="Navigation ButtonGroup App-backControl">
            <button class="Button Button--icon Navigation-drawer hasIcon" type="button" aria-label="打开导航抽屉">
                <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                <span class="Button-label"></span>
            </button>
        </div>
    </div>
    <div id="drawer" class="App-drawer">
        <header id="header" class="App-header navbar-fixed-top">
            <div id="header-navigation" class="Header-navigation">
                <div class="Navigation ButtonGroup"></div>
            </div>
            <div class="container">
                <!-- 网站标题 -->
                <div class="Header-title">
                    <a href="{{ route("/") }}" id="home-link">
                        <img src="/assets/logo-nehml3ze.png" alt="小丑路人的flarum社区" class="Header-logo">
                    </a>
                </div>
                <!-- 菜单栏 -->
                <div id="header-primary" class="Header-primary display-none">
                    <ul class="Header-controls">
                        <li class="item-link1">
                            <a class="LinksButton Button Button--link" rel="noopener noreferrer" target="_blank" href="https://bbs.cnpscy.com">
                                <i aria-hidden="true" class="icon fas fa-website Button-icon LinksButton-icon"></i>
                                <span class="LinksButton-title">在线的·小丑路人社区</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="header-secondary" class="Header-secondary">
                    <ul class="Header-controls">
                        <li class="item-search display-none">
                            <div role="search" aria-label="搜索论坛" class="Search active open focused">
                                <div class="Search-input">
                                    <input aria-label="搜索" class="FormControl" type="search" placeholder="搜索" />
                                </div>
                                <ul class="Dropdown-menu Search-results" style="max-height: 217px;" aria-live="polite">
                                    <li class="Dropdown-header">主题</li>
                                    <li class="active">
                                        <a href="/search">
                                            <i aria-hidden="true" class="icon fas fa-search Button-caret"></i> 搜索“XXX”
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/search">
                                            <i aria-hidden="true" class="icon fas fa-search Button-caret"></i> 搜索用户“XXX”
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="item-locale">
                            <div class="ButtonGroup Dropdown dropdown Dropdown--select itemCount2">
                                <button class="Dropdown-toggle Button Button--link" aria-haspopup="menu" aria-label="更改论坛语言" data-toggle="dropdown">
                                    <span class="Button-label">简体中文</span>
                                    <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu ">
                                    <li class="">
                                        <button class="hasIcon" type="button" active="">
                                            <i aria-hidden="true" class="icon fas fa-check Button-icon"></i>
                                            <span class="Button-label">简体中文</span>
                                        </button>
                                    </li>
                                    <li class="">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon Button-icon"></i>
                                            <span class="Button-label">English</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <!-- 未登录 -->
                        @if(!Auth::check())
                            <li class="item-nightmode">
                                <button class="Button Button--flat hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-adjust Button-icon"></i>
                                    <span class="Button-label">切换论坛主题</span>
                                </button>
                            </li>
                            <li class="item-signUp">
                                <button class="Button Button--link" type="button">
                                    <span class="Button-label">注册</span>
                                </button>
                            </li>
                            <li class="item-logIn">
                                <button class="Button Button--link" type="button" onclick="showLoginModal()">
                                    <span class="Button-label">登录</span>
                                </button>
                            </li>
                        @else
                            <!-- 已登录 -->
                            <li class="item-Drafts">
                                <div class="ButtonGroup Dropdown dropdown NotificationsDropdown itemCount0">
                                    <button class="Dropdown-toggle Button Button--flat" aria-haspopup="menu" aria-label="查看通知" data-toggle="dropdown" title="草稿">
                                        <i aria-hidden="true" class="icon fas fa-edit Button-icon"></i>
                                        <span class="Button-label">草稿</span>
                                    </button>
                                    <div class="Dropdown-menu Dropdown-menu--right"></div>
                                </div>
                            </li>
                            <li class="item-flags">
                                <div class="ButtonGroup Dropdown dropdown NotificationsDropdown itemCount0">
                                    <button class="Dropdown-toggle Button Button--flat" aria-haspopup="menu" aria-label="查看通知" data-toggle="dropdown" title="小黑屋">
                                        <i aria-hidden="true" class="icon fas fa-flag Button-icon"></i>
                                        <span class="NotificationsDropdown-unread">1</span>
                                        <span class="Button-label">小黑屋</span>
                                    </button>
                                    <div class="Dropdown-menu Dropdown-menu--right"></div>
                                </div>
                            </li>
                            <li class="item-nightmode">
                                <button class="Button Button--flat hasIcon" type="button">
                                    <i aria-hidden="true" class="icon far fa-sun Button-icon"></i>
                                    <span class="Button-label">切换论坛主题</span>
                                </button>
                            </li>
                            <li class="item-notifications">
                                <div class="ButtonGroup Dropdown dropdown NotificationsDropdown itemCount0">
                                    <button class="Dropdown-toggle Button Button--flat" aria-haspopup="menu" aria-label="查看通知" data-toggle="dropdown" title="通知中心">
                                        <i aria-hidden="true" class="icon fas fa-bell Button-icon"></i>
                                        <span class="Button-label">通知中心</span>
                                    </button>
                                    <div class="Dropdown-menu Dropdown-menu--right"></div>
                                </div>
                            </li>
                            <li class="item-session">
                            <div class="ButtonGroup Dropdown dropdown SessionDropdown itemCount7">
                                <button class="Dropdown-toggle Button Button--user Button--flat" aria-haspopup="menu" aria-label="会话下拉菜单开关" data-toggle="dropdown">
                                    <span class="Avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                    <span class="Button-label">
										<span class="username">{{ Auth::user()->userInfo->nick_name }}</span>
									</span>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                    <li class="item-profile">
                                        <a class="hasIcon" href="/u/admin" active="false">
                                            <i aria-hidden="true" class="icon fas fa-user Button-icon"></i>
                                            <span class="Button-label">个人主页</span>
                                        </a>
                                    </li>
                                    <li class="item-settings">
                                        <a class="hasIcon" href="/settings" active="false">
                                            <i aria-hidden="true" class="icon fas fa-cog Button-icon"></i>
                                            <span class="Button-label">设置</span>
                                        </a>
                                    </li>
                                    <li class="item-administration">
                                        <a class="hasIcon" href="/admin" target="_blank" active="false">
                                            <i aria-hidden="true" class="icon fas fa-wrench Button-icon"></i>
                                            <span class="Button-label">后台管理</span>
                                        </a>
                                    </li>
                                    <li class="item-clarkwinkelmann-status">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-grin Button-icon"></i>
                                            <span class="Button-label">设置状态</span>
                                        </button>
                                    </li>
                                    <li class="item-nightmode">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon far fa-moon Button-icon"></i>
                                            <span class="Button-label">夜间模式</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-logOut">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-sign-out-alt Button-icon"></i>
                                            <span class="Button-label">退出</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </header>
    </div>
</nav>
