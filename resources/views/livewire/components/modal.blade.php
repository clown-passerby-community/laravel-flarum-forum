@props(['id'])
@props(['content'])

<div id="{{ $id }}" class="display-none">
    <div class="ModalManager modal" data-modal-key="4" data-modal-number="0" role="dialog" aria-modal="true" style="--modal-number: 0;">
        <div class="Modal modal-dialog fade LogInModal Modal--small in">
            <div class="Modal-content">
                <div class="Modal-close App-backControl">
                    <button class="Button Button--icon Button--link hasIcon" type="button" aria-label="关闭" onclick="$(this).closest('#modal').html('');">
                        <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                        <span class="Button-label"></span>
                    </button>
                </div>
                <form>
                    {!! ($content) !!}
                </form>
            </div>
        </div>
        <div class="ModalManager-invisibleBackdrop"></div>
    </div>
    <div class="Modal-backdrop backdrop" style="--modal-count: 1;" data-showing=""></div>
</div>




