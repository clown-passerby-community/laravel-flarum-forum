<div class="UserDirectoryList UserDirectoryList--small-cards">
    <ul class="UserDirectoryList-users">
        <li data-id="1">
            <div class="User">
                <div class="UserCard UserCard--directory UserCard--small" style="--usercard-bg: #e5a0cd;">
                    <div class="darkenBackground">
                        <div class="container">
                            <div class="ButtonGroup Dropdown dropdown UserCard-controls App-primaryControl itemCount4">
                                <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="用户下拉菜单开关" data-toggle="dropdown">
                                    <i aria-hidden="true" class="icon fas fa-ellipsis-v Button-icon"></i>
                                    <span class="Button-label">操作</span>
                                    <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                    <li class="item-clarkwinkelmann-status">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-grin Button-icon"></i>
                                            <span class="Button-label">设置状态</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-edit">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                            <span class="Button-label">编辑</span>
                                        </button>
                                    </li>
                                    <li class="item-money">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-money-bill Button-icon"></i>
                                            <span class="Button-label">修改资产</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="UserCard-profile">
                                <h1 class="UserCard-identity">
                                    <a href="/u/admin">
                                        <div class="UserCard-avatar">
                                            <span loading="eager" class="Avatar" style="--avatar-bg: #e5a0cd; border-color: rgb(183, 42, 42);">小</span>
                                        </div>
                                        <span class="username">小丑路人</span>
                                    </a>
                                </h1>
                                <ul class="UserCard-badges badges">
                                    <li class="item-group1">
                                        <div class="Badge Badge--group--1 text-contrast--light" title="" aria-label="管理员" style="--badge-bg: #B72A2A;" data-original-title="管理员">
                                            <i aria-hidden="true" class="icon fas fa-wrench Badge-icon"></i>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="UserCard-info">
                                    <li class="item-joined">注册于 17 1月</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li data-id="2">
            <div class="User">
                <div class="UserCard UserCard--directory UserCard--small" style="--usercard-bg: #b1e5a0;">
                    <div class="darkenBackground">
                        <div class="container">
                            <div class="ButtonGroup Dropdown dropdown UserCard-controls App-primaryControl itemCount9">
                                <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="用户下拉菜单开关" data-toggle="dropdown">
                                    <i aria-hidden="true" class="icon fas fa-ellipsis-v Button-icon"></i>
                                    <span class="Button-label">操作</span>
                                    <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                    <li class="item-private-discussion">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                            <span class="Button-label">发送消息至 测试1</span>
                                        </button>
                                    </li>
                                    <li class="item-clarkwinkelmann-status">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-grin Button-icon"></i>
                                            <span class="Button-label">设置状态</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-edit">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                            <span class="Button-label">编辑</span>
                                        </button>
                                    </li>
                                    <li class="item-suspend">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-ban Button-icon"></i>
                                            <span class="Button-label">封禁</span>
                                        </button>
                                    </li>
                                    <li class="item-spammer">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pastafarianism Button-icon"></i>
                                            <span class="Button-label">垃圾用户</span>
                                        </button>
                                    </li>
                                    <li class="item-money">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-money-bill Button-icon"></i>
                                            <span class="Button-label">修改资产</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-delete">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                                            <span class="Button-label">删除</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="UserCard-profile">
                                <h1 class="UserCard-identity">
                                    <a href="/u/test001">
                                        <div class="UserCard-avatar">
                                            <span loading="eager" class="Avatar" style="--avatar-bg: #b1e5a0;">测</span>
                                        </div>
                                        <span class="username">测试1</span>
                                    </a>
                                </h1>
                                <ul class="UserCard-info">
                                    <li class="item-joined">注册于 18 1月</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li data-id="3">
            <div class="User">
                <div class="UserCard UserCard--directory UserCard--small" style="--usercard-bg: #e5c7a0;">
                    <div class="darkenBackground">
                        <div class="container">
                            <div class="ButtonGroup Dropdown dropdown UserCard-controls App-primaryControl itemCount9">
                                <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="用户下拉菜单开关" data-toggle="dropdown">
                                    <i aria-hidden="true" class="icon fas fa-ellipsis-v Button-icon"></i>
                                    <span class="Button-label">操作</span>
                                    <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                    <li class="item-private-discussion">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                            <span class="Button-label">发送消息至 liuneng</span>
                                        </button>
                                    </li>
                                    <li class="item-clarkwinkelmann-status">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-grin Button-icon"></i>
                                            <span class="Button-label">设置状态</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-edit">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                            <span class="Button-label">编辑</span>
                                        </button>
                                    </li>
                                    <li class="item-suspend">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-ban Button-icon"></i>
                                            <span class="Button-label">封禁</span>
                                        </button>
                                    </li>
                                    <li class="item-spammer">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pastafarianism Button-icon"></i>
                                            <span class="Button-label">垃圾用户</span>
                                        </button>
                                    </li>
                                    <li class="item-money">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-money-bill Button-icon"></i>
                                            <span class="Button-label">修改资产</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-delete">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                                            <span class="Button-label">删除</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="UserCard-profile">
                                <h1 class="UserCard-identity">
                                    <a href="/u/liuneng">
                                        <div class="UserCard-avatar">
                                            <span loading="eager" class="Avatar" style="--avatar-bg: #e5c7a0;">L</span>
                                        </div>
                                        <span class="username">liuneng</span>
                                    </a>
                                </h1>
                                <ul class="UserCard-info">
                                    <li class="item-joined">注册于 18 1月</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <div class="UserDirectoryList-loadMore"></div>
</div>
