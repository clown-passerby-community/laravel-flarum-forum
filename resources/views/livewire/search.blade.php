<div>
    <input wire:model.live="search">

    <button wire:click="resetQuery">Reset Search</button>

    <ul>
        @foreach($posts as $post)
            <li>
                {{$post->id}} . {{$post->title}}
                <livewire:delete-post :post="$post" />

            </li>
        @endforeach
    </ul>
</div>
