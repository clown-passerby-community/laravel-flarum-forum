<x-app-layout>
    <div class="UserPage">
        <div class="UserCard Hero UserHero" style="--usercard-bg: #b1e5a0;">
            <div class="darkenBackground">
                <div class="container">
                    <div class="ButtonGroup Dropdown dropdown UserCard-controls App-primaryControl itemCount9">
                        <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="用户下拉菜单开关" data-toggle="dropdown">
                            <i aria-hidden="true" class="icon fas fa-ellipsis-v Button-icon"></i>
                            <span class="Button-label">操作</span>
                            <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                        </button>
                        <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                            <li class="item-private-discussion">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                    <span class="Button-label">发送消息至 测试1</span>
                                </button>
                            </li>
                            <li class="item-clarkwinkelmann-status">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-grin Button-icon"></i>
                                    <span class="Button-label">设置状态</span>
                                </button>
                            </li>
                            <li class="Dropdown-separator"></li>
                            <li class="item-edit">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                    <span class="Button-label">编辑</span>
                                </button>
                            </li>
                            <li class="item-suspend">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-ban Button-icon"></i>
                                    <span class="Button-label">封禁</span>
                                </button>
                            </li>
                            <li class="item-spammer">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-pastafarianism Button-icon"></i>
                                    <span class="Button-label">垃圾用户</span>
                                </button>
                            </li>
                            <li class="item-money">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-money-bill Button-icon"></i>
                                    <span class="Button-label">修改资产</span>
                                </button>
                            </li>
                            <li class="Dropdown-separator"></li>
                            <li class="item-delete">
                                <button class="hasIcon" type="button">
                                    <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                                    <span class="Button-label">删除</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="UserCard-profile">
                        <h1 class="UserCard-identity">
                            <div class="AvatarEditor Dropdown UserCard-avatar">
                                <span loading="eager" class="Avatar" style="--avatar-bg: #b1e5a0;">测</span>
                                <a class="Dropdown-toggle AvatarEditor--noAvatar" title="上传新头像" data-toggle="dropdown">
                                    <i aria-hidden="true" class="icon fas fa-plus-circle"></i>
                                </a>
                                <ul class="Dropdown-menu Menu">
                                    <li class="item-upload">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-upload Button-icon"></i>
                                            <span class="Button-label">上传</span>
                                        </button>
                                    </li>
                                    <li class="item-remove">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                                            <span class="Button-label">移除</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <span class="username">测试1</span>
                        </h1>
                        <ul class="UserCard-info">
                            <li class="item-lastSeen">
							<span class="UserCard-lastSeen">
								<i aria-hidden="true" class="icon far fa-clock"></i> 18 1月
							</span>
                            </li>
                            <li class="item-joined">注册于 18 1月</li>
                            <li class="item-best-answer-count">
							<span class="UserCard-bestAnswerCount">
								<i aria-hidden="true" class="icon fas fa-check"></i>0 次助人
							</span>
                            </li>
                            <li class="item-fofsocialprofile">
                                <div class="Badge Badge--social null-social-settings text-contrast--unchanged" title="" aria-label="添加社交网络" style="" data-original-title="添加社交网络">
                                    <i aria-hidden="true" class="icon fas fa-plus Badge-icon"></i>
                                </div>
                            </li>
                            <li class="item-profile-views">
							<span>
								<i aria-hidden="true" class="icon far fa-eye"></i> 访问量 2
							</span>
                            </li>
                            <li class="item-money">
                                <span>🆚</span>
                            </li>
                            <li class="item-bio">
                                <div class="UserBio editable">
                                    <div class="UserBio-content" style="--bio-max-lines: 0;">
                                        <p class="UserBio-placeholder">为 测试1 设置个性签名</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="sideNavContainer">
                <nav class="sideNav UserPage-nav">
                    <ul class="">
                        <li class="item-nav">
                            <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount7">
                                <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown">
                                    <span class="Button-label">回复</span>
                                    <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu ">
                                    <li class="item-posts active">
                                        <a class="hasIcon" href="/u/test001" active="true">
                                            <i aria-hidden="true" class="icon far fa-comment Button-icon"></i>
                                            <span class="Button-label">回复 <span class="Button-badge">2</span>
										</span>
                                        </a>
                                    </li>
                                    <li class="item-discussions">
                                        <a class="hasIcon" href="/u/test001/discussions" active="false">
                                            <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                                            <span class="Button-label">主题 <span class="Button-badge">1</span>
										</span>
                                        </a>
                                    </li>
                                    <li class="item-likes">
                                        <a class="hasIcon" href="/u/test001/likes" active="false">
                                            <i aria-hidden="true" class="icon far fa-thumbs-up Button-icon"></i>
                                            <span class="Button-label">赞</span>
                                        </a>
                                    </li>
                                    <li class="item-byobu">
                                        <a class="hasIcon" href="/u/test001/private" active="false">
                                            <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                            <span class="Button-label">私密主题</span>
                                        </a>
                                    </li>
                                    <li class="item-mentions">
                                        <a class="hasIcon" href="/u/test001/mentions" name="mentions" active="false">
                                            <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                                            <span class="Button-label">被提及</span>
                                        </a>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-security">
                                        <a class="hasIcon" href="/u/test001/security" active="false">
                                            <i aria-hidden="true" class="icon fas fa-shield-alt Button-icon"></i>
                                            <span class="Button-label">Security</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="item-lastViewedUsers">
                            <fieldset class="LastUsers">
                                <legend>足迹</legend>
                                <ul>
                                    <li class="item-lastUser-0">
                                        <div class="item-lastUser-content">
                                            <span class="Avatar" loading="lazy"></span>
                                            <div>访客<span class="lastUser-visited" title="2024/3/8 15:08:31">1 分钟前</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-lastUser-1">
                                        <a href="/u/Admin">
                                            <div class="item-lastUser-content">
                                                <span class="Avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                                <div>Admin<span class="lastUser-visited" title="2024/1/18 15:25:10">18 1月</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </fieldset>
                        </li>
                    </ul>
                </nav>
                <div class="sideNavOffset UserPage-content">
                    <div class="PostsUserPage">
                        <ul class="PostsUserPage-list">
                            <li>
                                <div class="PostsUserPage-discussion">于 <a href="/d/3-002a">002啊</a>
                                </div>
                                <article class="CommentPost Post">
                                    <div>
                                        <header class="Post-header">
                                            <ul>
                                                <li class="item-user">
                                                    <div class="PostUser">
                                                        <h3 class="PostUser-name">
                                                            <a href="/u/test001">
                                                                <span class="Avatar PostUser-avatar" loading="lazy" style="--avatar-bg: #b1e5a0;">测</span>
                                                                <span class="username">测试1</span>
                                                            </a>
                                                        </h3>
                                                        <ul class="PostUser-badges badges"></ul>
                                                        <div class="PostUser-level" title="" aria-label="54 点经验" data-original-title="54 点经验">
														<span class="PostUser-text">
															<span class="PostUser-levelText">Lv.</span>&nbsp;<span class="PostUser-levelPoints">0</span>
														</span>
                                                            <div class="PostUser-bar PostUser-bar--empty"></div>
                                                            <div class="PostUser-bar" style="width: 40%;"></div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="item-meta">
                                                    <div class="Dropdown PostMeta">
                                                        <a class="Dropdown-toggle" data-toggle="dropdown">
                                                            <time pubdate="" datetime="2024-01-18T11:20:00+08:00" title="2024年1月18日星期四 11点20分" data-humantime="">18 1月</time>
                                                        </a>
                                                        <div class="Dropdown-menu dropdown-menu">
                                                            <span class="PostMeta-number">发布 #1</span>
                                                            <span class="PostMeta-time">
															<time pubdate="" datetime="2024-01-18T11:20:00+08:00">2024年1月18日星期四 11点20分</time>
														</span>
                                                            <span class="PostMeta-ip">
															<div class="ip-container">
																<div class="ip-info">
																	<span title="" aria-label="private range " data-original-title="private range ">172.19.0.1</span>
																</div>
															</div>
														</span>
                                                            <input class="FormControl PostMeta-permalink">
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </header>
                                        <div class="Post-body">
                                            <p>
                                                <img draggable="false" loading="lazy" class="emoji" alt="😅" src="https://cdn.jsdelivr.net/gh/twitter/twemoji@14/assets/72x72/1f605.png">表情包</p>
                                        </div>
                                        <aside class="Post-actions">
                                            <ul>
                                                <li class="item-react">
                                                    <div class="Reactions" style="margin-right: 7px;">
                                                        <div class="Reactions--reactions"></div>
                                                        <div class="Reactions--react">
                                                            <button class="Button Button--link Reactions--ShowReactions" type="Button" aria-label="戳表情">
															<span class="Button-label">
																<span class="Button-label">
																	<svg width="20px" height="20px" viewBox="0 0 18 18" class="button-react">
																		<g id="Reaction" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<g id="ic_reactions_grey">
																				<g id="Group-2">
																					<g id="0:0:0:0">
																						<rect id="Rectangle-5" x="0" y="0" width="18" height="18"></rect>
																						<g id="emoticon"></g>
																						<path d="M14.6332705,7.33333333 C14.6554304,7.55389388 14.6666667,7.77636769 14.6666667,8 C14.6666667,11.6818983 11.6818983,14.6666667 8,14.6666667 C6.23189007,14.6666667 4.53619732,13.9642877 3.28595479,12.7140452 C2.03571227,11.4638027 1.33333333,9.76810993 1.33333333,8 C1.33333333,4.33333333 4.31333333,1.33333333 8,1.33333333 L8,1.33333333 C8.22363231,1.33333333 8.44610612,1.3445696 8.66666667,1.36672949 L8.66666667,2.70847693 C8.44668912,2.68076722 8.22407146,2.66666667 8,2.66666667 C5.05448133,2.66666667 2.66666667,5.05448133 2.66666667,8 C2.66666667,10.9455187 5.05448133,13.3333333 8,13.3333333 C10.9455187,13.3333333 13.3333333,10.9455187 13.3333333,8 C13.3333333,7.77592854 13.3192328,7.55331088 13.2915231,7.33333333 L14.6332705,7.33333333 Z M8,11.6666667 C9.55333333,11.6666667 10.8666667,10.6933333 11.4066667,9.33333333 L4.59333333,9.33333333 C5.12666667,10.6933333 6.44666667,11.6666667 8,11.6666667 Z M10.3333333,7.33333333 C10.8856181,7.33333333 11.3333333,6.88561808 11.3333333,6.33333333 C11.3333333,5.78104858 10.8856181,5.33333333 10.3333333,5.33333333 C9.78104858,5.33333333 9.33333333,5.78104858 9.33333333,6.33333333 C9.33333333,6.88561808 9.78104858,7.33333333 10.3333333,7.33333333 L10.3333333,7.33333333 Z M5.66666667,7.33333333 C6.21895142,7.33333333 6.66666667,6.88561808 6.66666667,6.33333333 C6.66666667,5.78104858 6.21895142,5.33333333 5.66666667,5.33333333 C5.11438192,5.33333333 4.66666667,5.78104858 4.66666667,6.33333333 C4.66666667,6.88561808 5.11438192,7.33333333 5.66666667,7.33333333 Z" id="Combined-Shape" fill="#667c99"></path>
																					</g>
																					<g id="Group-15" transform="translate(10.666667, 0.000000)" fill="#667c99">
																						<polygon id="Path" points="3.33333333 2 3.33333333 0 2 0 2 2 0 2 0 3.33333333 2 3.33333333 2 5.33333333 3.33333333 5.33333333 3.33333333 3.33333333 5.33333333 3.33333333 5.33333333 2"></polygon>
																					</g>
																				</g>
																			</g>
																		</g>
																	</svg>
																</span>
															</span>
                                                            </button>
                                                            <div class="CommentPost--Reactions" style="">
                                                                <ul class="Reactions--Ul">
                                                                    <li class="item-thumbsup">
                                                                        <button class="Button Button--link" type="button" aria-label="thumbsup" data-reaction="thumbsup">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44d.png" loading="lazy" draggable="true" alt="thumbsup" title="thumbsup">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-thumbsdown">
                                                                        <button class="Button Button--link" type="button" aria-label="thumbsdown" data-reaction="thumbsdown">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44e.png" loading="lazy" draggable="true" alt="thumbsdown" title="thumbsdown">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-laughing">
                                                                        <button class="Button Button--link" type="button" aria-label="laughing" data-reaction="laughing">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f606.png" loading="lazy" draggable="true" alt="laughing" title="laughing">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-confused">
                                                                        <button class="Button Button--link" type="button" aria-label="confused" data-reaction="confused">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f615.png" loading="lazy" draggable="true" alt="confused" title="confused">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-heart">
                                                                        <button class="Button Button--link" type="button" aria-label="heart" data-reaction="heart">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/2764.png" loading="lazy" draggable="true" alt="heart" title="heart">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-tada">
                                                                        <button class="Button Button--link" type="button" aria-label="tada" data-reaction="tada">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f389.png" loading="lazy" draggable="true" alt="tada" title="tada">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="item-like">
                                                    <button class="Button Button--link" type="button">
                                                        <span class="Button-label">取消赞</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <div class="ButtonGroup Dropdown dropdown Post-controls itemCount6">
                                                        <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="帖子下拉菜单开关" data-toggle="dropdown">
                                                            <i aria-hidden="true" class="icon fas fa-ellipsis-h Button-icon"></i>
                                                            <span class="Button-label"></span>
                                                            <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                                        </button>
                                                        <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                                            <li class="item-flag">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-flag Button-icon"></i>
                                                                    <span class="Button-label">举报</span>
                                                                </button>
                                                            </li>
                                                            <li class="Dropdown-separator"></li>
                                                            <li class="item-edit">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                                                    <span class="Button-label">编辑</span>
                                                                </button>
                                                            </li>
                                                            <li class="item-addPoll">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-poll Button-icon"></i>
                                                                    <span class="Button-label">Add Poll</span>
                                                                </button>
                                                            </li>
                                                            <li class="Dropdown-separator"></li>
                                                            <li class="item-hide">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon far fa-trash-alt Button-icon"></i>
                                                                    <span class="Button-label">删除</span>
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </aside>
                                        <footer class="Post-footer">
                                            <ul>
                                                <li class="item-liked">
                                                    <div class="Post-likedBy">
                                                        <i aria-hidden="true" class="icon far fa-thumbs-up"></i>
                                                        <a href="/u/admin">你</a> 觉得很赞
                                                    </div>
                                                </li>
                                            </ul>
                                        </footer>
                                    </div>
                                </article>
                            </li>
                            <li>
                                <div class="PostsUserPage-discussion">于 <a href="/d/2-mou-ha-ha/3">某哈哈</a>
                                </div>
                                <article class="undefined Post--flagged CommentPost Post">
                                    <div>
                                        <div class="Post-flagged">
                                            <div class="Post-flagged-flags">
                                                <div class="Post-flagged-flag">由 <span class="username">小丑路人</span> 举报：不雅</div>
                                            </div>
                                            <div class="Post-flagged-actions">
                                                <div class="ButtonGroup">
                                                    <button class="Button hasIcon" type="button">
                                                        <i aria-hidden="true" class="icon far fa-trash-alt Button-icon"></i>
                                                        <span class="Button-label">删除</span>
                                                    </button>
                                                </div>
                                                <button class="Button hasIcon" type="button">
                                                    <i aria-hidden="true" class="icon far fa-eye-slash Button-icon"></i>
                                                    <span class="Button-label">撤销举报</span>
                                                </button>
                                            </div>
                                        </div>
                                        <header class="Post-header">
                                            <ul>
                                                <li class="item-user">
                                                    <div class="PostUser">
                                                        <h3 class="PostUser-name">
                                                            <a href="/u/test001">
                                                                <span class="Avatar PostUser-avatar" loading="lazy" style="--avatar-bg: #b1e5a0;">测</span>
                                                                <span class="username">测试1</span>
                                                            </a>
                                                        </h3>
                                                        <ul class="PostUser-badges badges"></ul>
                                                        <div class="PostUser-level" title="" aria-label="54 点经验" data-original-title="54 点经验">
														<span class="PostUser-text">
															<span class="PostUser-levelText">Lv.</span>&nbsp;<span class="PostUser-levelPoints">0</span>
														</span>
                                                            <div class="PostUser-bar PostUser-bar--empty"></div>
                                                            <div class="PostUser-bar" style="width: 40%;"></div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="item-meta">
                                                    <div class="Dropdown PostMeta">
                                                        <a class="Dropdown-toggle" data-toggle="dropdown">
                                                            <time pubdate="" datetime="2024-01-18T10:48:06+08:00" title="2024年1月18日星期四 10点48分" data-humantime="">18 1月</time>
                                                        </a>
                                                        <div class="Dropdown-menu dropdown-menu">
                                                            <span class="PostMeta-number">发布 #3</span>
                                                            <span class="PostMeta-time">
															<time pubdate="" datetime="2024-01-18T10:48:06+08:00">2024年1月18日星期四 10点48分</time>
														</span>
                                                            <span class="PostMeta-ip">
															<div class="ip-container">
																<div class="ip-info">
																	<span title="" aria-label="private range " data-original-title="private range ">172.19.0.1</span>
																</div>
															</div>
														</span>
                                                            <input class="FormControl PostMeta-permalink">
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </header>
                                        <div class="Post-body">
                                            <p>3213132<strong>2222</strong>
                                            </p>
                                        </div>
                                        <aside class="Post-actions">
                                            <ul>
                                                <li class="item-react">
                                                    <div class="Reactions" style="margin-right: 7px;">
                                                        <div class="Reactions--reactions"></div>
                                                        <div class="Reactions--react">
                                                            <button class="Button Button--link Reactions--ShowReactions" type="Button" aria-label="戳表情">
															<span class="Button-label">
																<span class="Button-label">
																	<svg width="20px" height="20px" viewBox="0 0 18 18" class="button-react">
																		<g id="Reaction" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<g id="ic_reactions_grey">
																				<g id="Group-2">
																					<g id="0:0:0:0">
																						<rect id="Rectangle-5" x="0" y="0" width="18" height="18"></rect>
																						<g id="emoticon"></g>
																						<path d="M14.6332705,7.33333333 C14.6554304,7.55389388 14.6666667,7.77636769 14.6666667,8 C14.6666667,11.6818983 11.6818983,14.6666667 8,14.6666667 C6.23189007,14.6666667 4.53619732,13.9642877 3.28595479,12.7140452 C2.03571227,11.4638027 1.33333333,9.76810993 1.33333333,8 C1.33333333,4.33333333 4.31333333,1.33333333 8,1.33333333 L8,1.33333333 C8.22363231,1.33333333 8.44610612,1.3445696 8.66666667,1.36672949 L8.66666667,2.70847693 C8.44668912,2.68076722 8.22407146,2.66666667 8,2.66666667 C5.05448133,2.66666667 2.66666667,5.05448133 2.66666667,8 C2.66666667,10.9455187 5.05448133,13.3333333 8,13.3333333 C10.9455187,13.3333333 13.3333333,10.9455187 13.3333333,8 C13.3333333,7.77592854 13.3192328,7.55331088 13.2915231,7.33333333 L14.6332705,7.33333333 Z M8,11.6666667 C9.55333333,11.6666667 10.8666667,10.6933333 11.4066667,9.33333333 L4.59333333,9.33333333 C5.12666667,10.6933333 6.44666667,11.6666667 8,11.6666667 Z M10.3333333,7.33333333 C10.8856181,7.33333333 11.3333333,6.88561808 11.3333333,6.33333333 C11.3333333,5.78104858 10.8856181,5.33333333 10.3333333,5.33333333 C9.78104858,5.33333333 9.33333333,5.78104858 9.33333333,6.33333333 C9.33333333,6.88561808 9.78104858,7.33333333 10.3333333,7.33333333 L10.3333333,7.33333333 Z M5.66666667,7.33333333 C6.21895142,7.33333333 6.66666667,6.88561808 6.66666667,6.33333333 C6.66666667,5.78104858 6.21895142,5.33333333 5.66666667,5.33333333 C5.11438192,5.33333333 4.66666667,5.78104858 4.66666667,6.33333333 C4.66666667,6.88561808 5.11438192,7.33333333 5.66666667,7.33333333 Z" id="Combined-Shape" fill="#667c99"></path>
																					</g>
																					<g id="Group-15" transform="translate(10.666667, 0.000000)" fill="#667c99">
																						<polygon id="Path" points="3.33333333 2 3.33333333 0 2 0 2 2 0 2 0 3.33333333 2 3.33333333 2 5.33333333 3.33333333 5.33333333 3.33333333 3.33333333 5.33333333 3.33333333 5.33333333 2"></polygon>
																					</g>
																				</g>
																			</g>
																		</g>
																	</svg>
																</span>
															</span>
                                                            </button>
                                                            <div class="CommentPost--Reactions" style="left: -28%;">
                                                                <ul class="Reactions--Ul">
                                                                    <li class="item-thumbsup">
                                                                        <button class="Button Button--link" type="button" aria-label="thumbsup" data-reaction="thumbsup">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44d.png" loading="lazy" draggable="true" alt="thumbsup" title="thumbsup">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-thumbsdown">
                                                                        <button class="Button Button--link" type="button" aria-label="thumbsdown" data-reaction="thumbsdown">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44e.png" loading="lazy" draggable="true" alt="thumbsdown" title="thumbsdown">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-laughing">
                                                                        <button class="Button Button--link" type="button" aria-label="laughing" data-reaction="laughing">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f606.png" loading="lazy" draggable="true" alt="laughing" title="laughing">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-confused">
                                                                        <button class="Button Button--link" type="button" aria-label="confused" data-reaction="confused">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f615.png" loading="lazy" draggable="true" alt="confused" title="confused">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-heart">
                                                                        <button class="Button Button--link" type="button" aria-label="heart" data-reaction="heart">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/2764.png" loading="lazy" draggable="true" alt="heart" title="heart">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                    <li class="item-tada">
                                                                        <button class="Button Button--link" type="button" aria-label="tada" data-reaction="tada">
																		<span class="Button-label">
																			<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f389.png" loading="lazy" draggable="true" alt="tada" title="tada">
																		</span>
                                                                        </button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="item-like">
                                                    <button class="Button Button--link" type="button">
                                                        <span class="Button-label">赞</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <div class="ButtonGroup Dropdown dropdown Post-controls itemCount6">
                                                        <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="帖子下拉菜单开关" data-toggle="dropdown">
                                                            <i aria-hidden="true" class="icon fas fa-ellipsis-h Button-icon"></i>
                                                            <span class="Button-label"></span>
                                                            <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                                        </button>
                                                        <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                                            <li class="item-flag">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-flag Button-icon"></i>
                                                                    <span class="Button-label">举报</span>
                                                                </button>
                                                            </li>
                                                            <li class="Dropdown-separator"></li>
                                                            <li class="item-edit">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                                                    <span class="Button-label">编辑</span>
                                                                </button>
                                                            </li>
                                                            <li class="item-addPoll">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon fas fa-poll Button-icon"></i>
                                                                    <span class="Button-label">Add Poll</span>
                                                                </button>
                                                            </li>
                                                            <li class="Dropdown-separator"></li>
                                                            <li class="item-hide">
                                                                <button class="hasIcon" type="button">
                                                                    <i aria-hidden="true" class="icon far fa-trash-alt Button-icon"></i>
                                                                    <span class="Button-label">删除</span>
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </aside>
                                        <footer class="Post-footer"></footer>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="PostsUserPage-loadMore"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
