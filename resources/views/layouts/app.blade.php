<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="description" content="小丑路人的flarum社区">

        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <link rel="stylesheet" media="" class="nightmode-light" href="/assets/forum.css?v=618ffa2d">
        <!-- 引入jquery -->
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- 加载多模块化的js与scss -->
        <!--
        @vite(['app/Modules/Forum/resources/assets/sass/app.scss', 'app/Modules/Forum/resources/assets/js/app.js'])
        @vite(['app/Modules/Comment/resources/assets/sass/app.scss', 'app/Modules/Comment/resources/assets/js/app.js'])
        -->

        <!-- 一键加载多模块的css与js -->
        @vite(\Nwidart\Modules\Module::getAssets())
        <script>
            /* fof/nightmode workaround for browsers without (prefers-color-scheme) CSS media query support */
            if (!window.matchMedia('not all and (prefers-color-scheme), (prefers-color-scheme)').matches) {
                document.querySelector('link.nightmode-light').removeAttribute('media');
            }
        </script>

        @livewireStyles
    </head>
    <body class="no-touch">
        <div id="app" class="App affix App--index" aria-hidden="false">
            <livewire:layout.navigation />

            <!-- Page Content -->
            <main class="App-content">
                <div id="content">
                    {{ $slot }}
                </div>

                <!-- 发布动态弹层 -->
                <!--
                <livewire:dynamic.publish />
                -->
            </main>

            <div class="other-content">
                <!--
                <livewire:auth.login />
                -->
            </div>
        </div>
        <div id="modal"></div>
        <div id="alerts"><div class="AlertManager"></div></div>
        <script>
            // document.getElementById('flarum-loading').style.display = 'block';
            var flarum = {extensions: {}};
        </script>

        <script src="/assets/forum.js?v=c0aacec5"></script>
        <script src="/assets/forum-zh-Hans.js?v=6817002c"></script>
        <script>
            <!-- 设置`app`整体高度 -->
            const heroHeight = $('.Hero').outerHeight() || 0;
            $('#app').css('min-height', ($(window).height() || 0) + heroHeight);

            // const data = JSON.parse(document.getElementById('flarum-json-payload').textContent);
            // document.getElementById('flarum-loading').style.display = 'none';

            // try {
            //     flarum.core.app.load(data);
            //     flarum.core.app.bootExtensions(flarum.extensions);
            //     flarum.core.app.boot();
            // } catch (e) {
            //     var error = document.getElementById('flarum-loading-error');
            //     error.innerHTML += document.getElementById('flarum-content').textContent;
            //     error.style.display = 'block';
            //     throw e;
            // }

            $(function (){
                $("div.Search-input input").on("focus", function() {
                    console.log(222);
                    $(this).parent().parent().addClass('focused');
                });
            });
        </script>
        @livewireScripts
    </body>
</html>
