import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import collectModuleAssetsPaths from './vite-module-loader.js';
import path from 'path';
import {fileURLToPath} from 'url';
import fs from 'fs/promises';

// const __filename = fileURLToPath(import.meta.url);
// console.log(__filename)
//
// // 👇️ "/home/borislav/Desktop/javascript"
// const __dirname = path.dirname(__filename);
// console.log('directory-name 👉️', __dirname);


// export default defineConfig({
//     plugins: [
//         laravel({
//             input: [
//                 'resources/css/app.css',
//                 'resources/js/app.js',
//             ],
//             refresh: true,
//         })
//     ],
// });

// // 模块化的目录
// const modulesPath = 'app/Modules';
// // 自动读取模块下的scss与js
// const moduleStatusesPath = path.join(__dirname, 'modules_statuses.json');
// console.log(moduleStatusesPath);
// try{
//     // Read module_statuses.json
//     const moduleStatusesContent = await fs.readFile(moduleStatusesPath, 'utf-8');
//     // console.log(moduleStatusesContent);
//     const moduleStatuses = JSON.parse(moduleStatusesContent);
//     console.log(moduleStatuses);
//
//     const moduleDirectories = await fs.readdir(modulesPath);
//
//     for (const moduleDir of moduleDirectories) {
//         if (moduleDir === '.DS_Store') {
//             // Skip .DS_Store directory
//             continue;
//         }
//         console.log(moduleDir);
//         // Check if the module is enabled (status is true)
//         if (moduleStatuses[moduleDir] === true) {
//             const viteConfigPath = path.join(modulesPath, moduleDir, 'vite.config.js');
//             console.log(viteConfigPath)
//             const stat = await fs.stat(viteConfigPath);
//
//             if (stat.isFile()) {
//                 // Import the module-specific Vite configuration
//                 const moduleConfig = await import(viteConfigPath);
//                 // const moduleConfig = await fs.readFile(viteConfigPath, 'utf-8');
//
//                 console.log(moduleConfig.paths);
//
//                 // if (moduleConfig.paths && Array.isArray(moduleConfig.paths)) {
//                 //     paths.push(...moduleConfig.paths);
//                 // }
//             }
//         }
//     }
// } catch (error) {
//     console.error(`"vite.config.js" => Error reading module statuses or module configurations: ${error}`);
// }

// async function getConfig() {
//     const paths = [
//         'resources/css/app.css',
//         'resources/js/app.js',
//         // 'app/Modules/Forum/resources/assets/sass/app.scss',
//         // 'app/Modules/Forum/resources/assets/js/app.js',
//     ];
//     console.log(__dirname)
//     // 自动读取模块下的scss与js
//     const moduleStatusesPath = path.join(__dirname, 'modules_statuses.json');
//     // console.log(moduleStatusesPath);
//
//     // 追加的`paths`必须移除项目绝对路径
//     const allPaths = await collectModuleAssetsPaths(paths, 'app/Modules');
//     console.log(allPaths);
//     // // 移出的字符串
//     // const filterString = __dirname;
//     // const newPaths = [];
//     // for (const path of allPaths) {
//     //     newPaths.push(path.replace(filterString + '/', ''));
//     // }
//     // console.log(newPaths);
//     // // const filteredArr = allPaths.filter(item => item.replace(filterString, ''));
//     // // console.log(filteredArr);
//
//     return defineConfig({
//         plugins: [
//             laravel({
//                 input: allPaths,
//                 refresh: true,
//             })
//         ]
//     });
// }
//
// export default getConfig();

// 此配置必须与PHP的模块化配置的目录一致（config => modules.paths.modules）
const modulesPath = 'app/Modules';

async function getConfig() {
    const paths = [
        'resources/css/app.css',
        'resources/js/app.js',
    ];
    const allPaths = await collectModuleAssetsPaths(paths, modulesPath);

    console.log(allPaths);

    return defineConfig({
        plugins: [
            laravel({
                input: allPaths,
                refresh: true,
            })
        ]
    });
}

export default getConfig();
