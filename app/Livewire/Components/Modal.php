<?php

namespace App\Livewire\Components;

use Livewire\Component;

// 模态框
class Modal extends Component
{
    public $id;
    public $content;

    public function mount($id, $content = '')
    {
        $this->id = $id;
        $this->content = $content;
    }

    public function render()
    {
        return view('livewire.components.modal');
    }
}
