<?php

namespace App\Livewire;

use App\Models\Topic;
use Livewire\Component;
use \Illuminate\Session\SessionManager;
use Livewire\Attributes\Js;

class ShowPosts extends Component
{
    public Topic $post;

    public $title;
    public $content;

    public $query = '';

    #[Js]
    public function resetQuery()
    {
        return <<<'JS'
            $wire.query = '';
        JS;
    }

    // public function mount($id)
    // {
    //     $this->post = Topic::find($id);
    // }
    // public function mount(SessionManager $session, $post)
    // {
    //     $session->put("post.{$post->id}.last_viewed", now());
    //
    //     $this->title = $post->title;
    //     $this->content = $post->content;
    // }

    public function getPostCount()
    {
        return Topic::count();
    }

    public function like()
    {
        $this->post->addLikeBy(auth()->user());
    }

    public function render()
    {
        return view('livewire.show-posts', [
            'posts' => Topic::where(function ($query){
                if ($this->query){
                    $query->where('title', 'LIKE', '%' . trim($this->query) . '%');
                }
            })->limit(10)->orderByDesc('id')->get(),
        ]);
    }
}
