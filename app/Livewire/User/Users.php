<?php

namespace App\Livewire\User;

use App\Models\Topic;
use Livewire\Component;
use Livewire\WithPagination;

class Users extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var array
     */
    protected $queryString = [
        'q' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    /**
     * 初始化.
     */
    public function mount(): void
    {
        $this->fill(request()->only('q', 'page'));
    }

    public function render()
    {
        $users = Topic::paginate(10);
        return view('livewire.user.users', compact('users'));
    }
}
