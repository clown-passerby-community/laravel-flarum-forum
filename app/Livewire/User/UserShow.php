<?php

namespace App\Livewire\User;

use Livewire\Component;

class UserShow extends Component
{
    public function render()
    {
        return view('livewire.user.user-show');
    }
}
