<?php

namespace App\Livewire;

use App\Models\Topic;
use Livewire\Component;
use Livewire\Attributes\Js;

class Search extends Component
{
    public $search = '';
    public $posts;

    #[Js]
    public function resetQuery()
    {
        return <<<'JS'
            $wire.search = '';
        JS;
    }

    public function render()
    {
        $this->posts = Topic::where('title', 'LIKE', '%' . trim($this->search) . '%')->limit(10)->orderByDesc('id')->get();
        return view('livewire.search');
    }
}
