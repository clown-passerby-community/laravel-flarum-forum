<?php

namespace App\Livewire;

use App\Models\Topic;
use Livewire\Component;
use \Illuminate\Session\SessionManager;

class DeletePost extends Component
{
    public Topic $post;

    public $title;
    public $content;

    public function delete()
    {
        $this->post->delete();
    }

    public function render()
    {
        return <<<'blade'
            <button
                type="button"
                wire:click="delete"
                wire:confirm="Are you sure you want to delete this post?"
            >
                Delete post 弹窗选择
            </button>

        blade;
    }
}
