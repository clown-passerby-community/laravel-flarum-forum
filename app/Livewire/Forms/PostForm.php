<?php

namespace App\Livewire\Forms;

use App\Models\Topic;
use Livewire\Attributes\Rule;
use Livewire\Attributes\Validate;
use Livewire\Form;

class PostForm extends Form
{
    public ?Topic $post;

    #[Validate('required|min:5')]
    public $title = '';

    public $content = '';

    public function rules()
    {
        return [
            'title' => [
                'required',
                Rule::unique('topics')->ignore($this->post),
            ],
            'content' => 'required|min:5',
        ];
    }

    public function store()
    {
        $this->validate();

        Topic::create([
            'user_id' => rand(1, 99999),
            'title' => $this->title,
            'content' => $this->content,
        ]);

        $this->reset();
    }

    public function update()
    {
        $this->validate();

        $this->post->update($this->all());

        $this->reset();
    }
}
