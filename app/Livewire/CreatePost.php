<?php

namespace App\Livewire;

use App\Livewire\Forms\PostForm;
use App\Models\Topic;
use Livewire\Component;
use Livewire\Attributes\Validate;

class CreatePost extends Component
{
    public PostForm $form;

    public function save()
    {
        $this->validate();

        $this->form->store();

        return $this->redirect('/posts');
        // return redirect()->to('/posts')
        //     ->with('status', 'Post successfully created.');
    }

    public function render()
    {
        return view('livewire.create-post');
    }
}
