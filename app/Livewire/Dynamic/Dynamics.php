<?php

namespace App\Livewire\Dynamic;

use App\Models\Topic;
use Livewire\Component;
use Livewire\WithPagination;

class Dynamics extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var array
     */
    protected $queryString = [
        'q' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    /**
     * 初始化.
     */
    public function mount(): void
    {
        $this->fill(request()->only('q', 'page'));
    }

    public function render()
    {
        $topics = Topic::paginate(10);
        return view('livewire.dynamic.dynamics', compact('topics'));
    }
}
