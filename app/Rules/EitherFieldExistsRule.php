<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Contracts\Validation\Rule;

// 字段二选一必须存在
class EitherFieldExistsRule implements Rule
{
    protected $fieldA;
    protected $fieldB;

    public function __construct($fieldA, $fieldB)
    {
        $this->fieldA = $fieldA;
        $this->fieldB = $fieldB;
    }

    public function passes($attribute, $value)
    {
        $data = request()->all();
        return array_key_exists($this->fieldA, $data) || array_key_exists($this->fieldB, $data);
    }

    public function message()
    {
        return 'Either :attribute or ' . $this->fieldB . ' must be present.';
    }
}
