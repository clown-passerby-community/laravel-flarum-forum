<?php

namespace App\Modules\Topic\App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Forum\App\Models\PlatformEvent;
use App\Modules\Topic\App\Models\Topic;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    public function index()
    {
        $topics = Topic::get();
        return view('topic::index', compact('topics'));
    }

    /**
     * Show the specified resource.
     */
    public function show($topic_id, Request $request)
    {
        $topic = Topic::findOrFail($topic_id);

        // 记录`访问话题详情`事件
        PlatformEvent::record(PlatformEvent::EVENT_VISIT_TOPIC, [
            'login_user_id' => Auth::id(),
            'topic_id' => $topic->topic_id,
            'created_ip'   => $request->ip(),
            'browser_type' => $request->userAgent(),
        ]);

        return view('topic::show', compact('topic'));
    }
}
