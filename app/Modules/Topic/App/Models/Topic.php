<?php

namespace App\Modules\Topic\App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Modules\Topic\Database\factories\TopicFactory;

class Topic extends Model
{
    protected $primaryKey = 'topic_id';
    protected $is_delete  = 0;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [];

    protected static function newFactory(): TopicFactory
    {
        //return TopicFactory::new();
    }
}
