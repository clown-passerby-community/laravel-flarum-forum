<x-app-layout>
    <div class="TagsPage">
        <header class="Hero WelcomeHero">
            <div class="container">
                <button class="Hero-close Button Button--icon Button--link hasIcon" type="button" aria-label="隐藏欢迎横幅内容">
                    <i aria-hidden="true" class="icon fas fa-times Button-icon"></i>
                    <span class="Button-label"></span>
                </button>
                <div class="containerNarrow">
                    <h1 class="Hero-title">Welcome to 小丑路人的flarum社区</h1>
                    <div class="Hero-subtitle">Enjoy your new forum! Hop over to discuss.flarum.org if you have any questions, or to join our community!</div>
                </div>
            </div>
        </header>
        <div class="container">
            <nav class="TagsPage-nav IndexPage-nav sideNav">
                <ul>
                    <li class="item-newDiscussion App-primaryControl">
                        <button class="Button Button--primary IndexPage-newDiscussion hasIcon" type="button" itemclassname="App-primaryControl">
                            <i aria-hidden="true" class="icon fas fa-edit Button-icon"></i>
                            <span class="Button-label">发布主题</span>
                        </button>
                    </li>
                    <li class="item-nav">
                        <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount2">
                            <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="导航栏下拉菜单开关" data-toggle="dropdown">
                                <span class="Button-label">标签</span>
                                <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                            </button>
                            <ul class="Dropdown-menu dropdown-menu ">
                                <li class="item-allDiscussions">
                                    <a class="hasIcon" href="/" active="false">
                                        <i aria-hidden="true" class="icon far fa-comments Button-icon"></i>
                                        <span class="Button-label">全部主题</span>
                                    </a>
                                </li>
                                <li class="item-tags active">
                                    <a class="hasIcon" href="/tags" active="true">
                                        <i aria-hidden="true" class="icon fas fa-th-large Button-icon"></i>
                                        <span class="Button-label">标签</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="TagsPage-content sideNavOffset">
                <ul class="TagTiles">
                    @foreach($topics as $topic)
                        <li class="TagTile text-contrast--unchanged" style="">
                            <a class="TagTile-info" href="{{ route('topic.show', [$topic->topic_id]) }}">
                                <h3 class="TagTile-name">{{ $topic->topic_name }}</h3>
                                <p class="TagTile-description">{{ $topic->topic_description }}</p>
                                <div class="TagTile-children"></div>
                            </a>
                            <a class="TagTile-lastPostedDiscussion" href="/d/21-011">
                                <span class="TagTile-lastPostedDiscussion-title">011</span>
                                <time pubdate="" datetime="2024-01-18T16:41:23+08:00" title="2024年1月18日星期四 16点41分" data-humantime="">18 1月</time>
                            </a>
                        </li>
                        <li class="TagTile colored text-contrast--light" style="--tag-bg: #888;">
                            <a class="TagTile-info" href="{{ route('topic.show', [$topic->topic_id]) }}">
                                <h3 class="TagTile-name">{{ $topic->topic_name }}</h3>
                                <p class="TagTile-description">{{ $topic->topic_description }}</p>
                                <div class="TagTile-children"></div>
                            </a>
                            <a class="TagTile-lastPostedDiscussion" href="/d/28-da-pian-fu-de-wen-zhang-nei-rong/3">
                                <span class="TagTile-lastPostedDiscussion-title">大篇幅的文章内容</span>
                                <time pubdate="" datetime="2024-02-29T17:13:44+08:00" title="2024年2月29日星期四 17点13分" data-humantime="">22 天前</time>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</x-app-layout>
