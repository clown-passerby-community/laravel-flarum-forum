<x-app-layout>
    <div class="IndexPage IndexPage--tag3">
        <header class="Hero TagHero TagHero--colored text-contrast--light" style="--hero-bg: #421414;">
            <div class="container">
                <div class="containerNarrow">
                    <h1 class="Hero-title">{{ $topic->topic_name }}</h1>
                    <div class="Hero-subtitle">{{ $topic->topic_description }}</div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="sideNavContainer">
                <nav class="IndexPage-nav sideNav">
                    <ul>
                        <li class="item-newDiscussion App-primaryControl">
                            <button class="Button Button--primary IndexPage-newDiscussion Button--tagColored text-contrast--light hasIcon" type="button" itemclassname="App-primaryControl" style="--color: #421414;">
                                <i aria-hidden="true" class="icon fas fa-edit Button-icon"></i>
                                <span class="Button-label">发布主题</span>
                            </button>
                        </li>
                        <li class="item-subscriptionButton">
                            <button class="Button App-primaryControl SubscriptionButton SubscriptionButton--false hasIcon" type="button" title="" aria-label="有新主题或新回复时发送邮件提醒" data-original-title="有新主题或新回复时发送邮件提醒">
                                <i aria-hidden="true" class="icon fas fa-star Button-icon"></i>
                                <span class="Button-label">关注</span>
                            </button>
                        </li>
                        <li class="item-nav">
                            <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount9">
                                <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="导航栏下拉菜单开关" data-toggle="dropdown">
                                    <span class="Button-label">{{ $topic->topic_name }}</span>
                                    <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu ">
                                    <li class="item-allDiscussions">
                                        <a class="hasIcon" href="/" active="false">
                                            <i aria-hidden="true" class="icon far fa-comments Button-icon"></i>
                                            <span class="Button-label">全部主题</span>
                                        </a>
                                    </li>
                                    <li class="item-fof-user-directory">
                                        <a class="hasIcon" href="/users" active="false">
                                            <i aria-hidden="true" class="icon far fa-address-book Button-icon"></i>
                                            <span class="Button-label">会员名录</span>
                                        </a>
                                    </li>
                                    <li class="item-privateDiscussions">
                                        <a class="hasIcon" href="/private" active="false">
                                            <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                            <span class="Button-label">私密主题</span>
                                        </a>
                                    </li>
                                    <li class="item-following">
                                        <a class="hasIcon" href="/following" active="false">
                                            <i aria-hidden="true" class="icon fas fa-star Button-icon"></i>
                                            <span class="Button-label">我的关注</span>
                                        </a>
                                    </li>
                                    <li class="item-tags">
                                        <a class="hasIcon" href="/tags" active="false">
                                            <i aria-hidden="true" class="icon fas fa-th-large Button-icon"></i>
                                            <span class="Button-label">标签</span>
                                        </a>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-tag1">
                                        <a class="TagLinkButton hasIcon" href="/t/default" style="--color: #888;">
                                            <span class="Button-icon icon TagIcon" style="--color: #888;"></span>
                                            <span class="Button-label">默认</span>
                                        </a>
                                    </li>
                                    <li class="item-tag2">
                                        <a class="TagLinkButton hasIcon" href="/t/Technical-articles" title="1" style="">
                                            <span class="Button-icon icon TagIcon" style=""></span>
                                            <span class="Button-label">技术文章</span>
                                        </a>
                                    </li>
                                    <li class="item-tag3 active">
                                        <a class="TagLinkButton hasIcon" href="/t/aaa" style="--color: #421414;">
                                            <span class="Button-icon icon TagIcon" style="--color: #421414;"></span>
                                            <span class="Button-label">娱乐</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="item-forumStatisticsWidget">
                            <div class="ForumStatistics containerNarrow">
                                <div class="row">
                                    <h2>
                                        <i class="fas fa-chart-bar"></i> 论坛统计
                                    </h2>
                                    <div>
                                        <ul id="ForumStatisticsList">
                                            <li>主题数：25</li>
                                            <li>回复数：36</li>
                                            <li>会员数：3</li>
                                            <li>最近注册： <a href="/u/liuneng">
                                                    <strong>
                                                        <span class="username">liuneng</span>
                                                    </strong>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="item-onlineUsers">
                            <div class="OnlineUsers">
                                <div class="OnlineUsers-title">在线会员</div>
                                <div class="OnlineUsers-list">
                                    <div class="OnlineUsers-item">
                                        <a href="/u/admin" title="小丑路人">
                                            <span class="Avatar OnlineUsers-avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                            <span class="username">小丑路人</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
                <div class="IndexPage-results sideNavOffset">
                    <div class="IndexPage-toolbar">
                        <ul class="IndexPage-toolbar-view">
                            <li class="item-sort">
                                <div class="ButtonGroup Dropdown dropdown  itemCount6">
                                    <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="更改「全部主题」排序" data-toggle="dropdown">
                                        <span class="Button-label">最新回复</span>
                                        <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                    </button>
                                    <ul class="Dropdown-menu dropdown-menu ">
                                        <li class="">
                                            <button class="hasIcon" type="button" active="">
                                                <i aria-hidden="true" class="icon fas fa-check Button-icon"></i>
                                                <span class="Button-label">最新回复</span>
                                            </button>
                                        </li>
                                        <li class="">
                                            <button class="hasIcon" type="button">
                                                <i aria-hidden="true" class="icon Button-icon"></i>
                                                <span class="Button-label">热门主题</span>
                                            </button>
                                        </li>
                                        <li class="">
                                            <button class="hasIcon" type="button">
                                                <i aria-hidden="true" class="icon Button-icon"></i>
                                                <span class="Button-label">新鲜出炉</span>
                                            </button>
                                        </li>
                                        <li class="">
                                            <button class="hasIcon" type="button">
                                                <i aria-hidden="true" class="icon Button-icon"></i>
                                                <span class="Button-label">陈年旧贴</span>
                                            </button>
                                        </li>
                                        <li class="">
                                            <button class="hasIcon" type="button">
                                                <i aria-hidden="true" class="icon Button-icon"></i>
                                                <span class="Button-label">最多翻阅</span>
                                            </button>
                                        </li>
                                        <li class="">
                                            <button class="hasIcon" type="button">
                                                <i aria-hidden="true" class="icon Button-icon"></i>
                                                <span class="Button-label">最少翻阅</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <ul class="IndexPage-toolbar-action">
                            <li class="item-markAllAsRead">
                                <button class="Button Button--icon hasIcon" type="button" aria-label="将所有帖子标记为已读">
                                    <i aria-hidden="true" class="icon fas fa-check Button-icon"></i>
                                    <span class="Button-label"></span>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!-- 动态列表：如果需读取的是模块化的`livewire`组件，目前只能使用指定class；如果是直接调用默认路径的组件：`<livewire:auth.login />` -->
                    {!! \Livewire\Livewire::mount(App\Modules\Forum\App\Livewire\Dynamic\Lists::class, ['topic_id' => $topic->topic_id]); !!}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
