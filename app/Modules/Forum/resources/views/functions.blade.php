<x-app-layout>
    <div class="container row">
        <div class="card">
            <div class="card-body">
                <div id="test-editormd-view">
                    <textarea style="display:none;" name="test-editormd-markdown-doc"></textarea>
                </div>
            </div>
        </div>
        <div class="display-none content">{!! $functions !!}</div>
    </div>
    {!! editor_css() !!}
    {!! editor_only_js() !!}
    <script>
        var testEditor = editormd.markdownToHTML("test-editormd-view", {
            markdown        : "\r\n" + $("div.content").text(),//+ "\r\n" + $("#append-test").text(),
            //htmlDecode      : true,       // 开启 HTML 标签解析，为了安全性，默认不开启
            htmlDecode      : "style,script,iframe",  // you can filter tags decode
            //toc             : false,
            tocm            : true,    // Using [TOCM]
            // tocContainer    : "#custom-toc-container", // 自定义 ToC 容器层
            emoji           : true,
            taskList        : true,
            tex             : true,  // 默认不解析
            flowChart       : true,  // 默认不解析
            sequenceDiagram : true,  // 默认不解析
        });
    </script>
</x-app-layout>
