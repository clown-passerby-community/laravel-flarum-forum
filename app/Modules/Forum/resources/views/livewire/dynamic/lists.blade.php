<div>
    @if($dynamics->total() > 0)
        <div class="DiscussionList">
            <ul role="feed" class="DiscussionList-discussions">
                @foreach($dynamics as $dynamic)
                    <li data-id="{{$dynamic->dynamic_id}}" role="article" aria-setsize="-1" aria-posinset="0">
                        <div class="DiscussionListItem">
                            <div class="ButtonGroup Dropdown dropdown DiscussionListItem-controls itemCount9">
                                <button class="Dropdown-toggle Button Button--icon Button--flat Slidable-underneath Slidable-underneath--right" aria-haspopup="menu" aria-label="主题下拉菜单开关" data-toggle="dropdown">
                                    <i aria-hidden="true" class="icon fas fa-ellipsis-v Button-icon"></i>
                                    <span class="Button-label"></span>
                                    <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                </button>
                                <ul class="Dropdown-menu dropdown-menu ">
                                    <li class="item-subscription">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-star Button-icon"></i>
                                            <span class="Button-label">关注</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-rename">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                            <span class="Button-label">重命名</span>
                                        </button>
                                    </li>
                                    <li class="item-tags">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-tag Button-icon"></i>
                                            <span class="Button-label">编辑标签</span>
                                        </button>
                                    </li>
                                    <li class="item-reset">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon far fa-eye-slash Button-icon"></i>
                                            <span class="Button-label">重置浏览量</span>
                                        </button>
                                    </li>
                                    <li class="item-sticky">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-thumbtack Button-icon"></i>
                                            <span class="Button-label">置顶主题</span>
                                        </button>
                                    </li>
                                    <li class="item-lock">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon fas fa-lock Button-icon"></i>
                                            <span class="Button-label">锁定主题</span>
                                        </button>
                                    </li>
                                    <li class="Dropdown-separator"></li>
                                    <li class="item-hide">
                                        <button class="hasIcon" type="button">
                                            <i aria-hidden="true" class="icon far fa-trash-alt Button-icon"></i>
                                            <span class="Button-label">删除</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <span class="Slidable-underneath Slidable-underneath--left Slidable-underneath--elastic disabled">
                                        <i aria-hidden="true" class="icon fas fa-check"></i>
                                    </span>
                            <div class="DiscussionListItem-content Slidable-content read">
                                <a class="DiscussionListItem-author" href="/u/admin" title="" aria-label="小丑路人 发布于 3 天前" data-original-title="小丑路人 发布于 3 天前">
                                    <span class="Avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                </a>
                                <ul class="DiscussionListItem-badges badges"></ul>
                                <a href="{{ route('show', ['dynamic_id' => $dynamic->dynamic_id]) }}" class="DiscussionListItem-main">
                                    <h2 class="DiscussionListItem-title">{{$dynamic->dynamic_title}}</h2>
                                    <ul class="DiscussionListItem-info">
                                        <li class="item-tags">
                                        <span class="TagsLabel">
                                            <span class="TagLabel  colored text-contrast--light" style="--tag-bg: #888;">
                                                <span class="TagLabel-text">
                                                    <span class="TagLabel-name">默认</span>
                                                </span>
                                            </span>
                                        </span>
                                        </li>
                                        <li class="item-terminalPost">
                                        <span>
                                            <span class="username">小丑路人</span> 发布于 <time pubdate="" datetime="2024-02-26T16:45:03+08:00" title="2024年2月26日星期一 16点45分" data-humantime="">3 天前</time>
                                        </span>
                                        </li>
                                        <li class="item-discussion-views">
                                            <span>1</span>
                                        </li>
                                    </ul>
                                </a>
                                <span class="DiscussionListItem-count">
                                <span aria-hidden="true">0</span>
                                <span class="visually-hidden">0 条回复</span>
                            </span>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>

            {{ $dynamics->onEachSide(2)->render() }}
        </div>
    @else
        <div class="DiscussionList">
            <div class="Placeholder">
                <p>这里空空如也...</p>
            </div>
        </div>
    @endif
</div>
