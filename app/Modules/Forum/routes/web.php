<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Forum\App\Http\Controllers\ForumController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('')->middleware([])->group(function () {
    // 查看已有的功能列表
    Route::get('functions', [ForumController::class, 'functions'])->name('functions');

    Route::get('/', [ForumController::class, 'index'])->name('/');
    Route::get('/dynamic/{dynamic_id}', [ForumController::class, 'show'])->name('show');
});
