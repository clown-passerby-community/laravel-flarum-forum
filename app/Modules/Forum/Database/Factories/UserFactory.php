<?php

namespace App\Modules\Forum\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     */
    protected $model = \App\Modules\Forum\App\Models\User::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'user_mobile' => $this->faker->unique()->numberBetween(13000000001, 19099999999),
            'user_name' => $this->faker->unique()->userName,
            'user_email' => $this->faker->unique()->email,
            'password' => hash_make(123456),
        ];
    }
}

