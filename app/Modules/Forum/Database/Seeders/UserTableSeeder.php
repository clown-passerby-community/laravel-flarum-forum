<?php

namespace App\Modules\Forum\Database\Seeders;

use App\Modules\Forum\App\Models\User;
use App\Modules\Forum\App\Models\UserInfo;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->count(100)->create()->each(function ($user){
            $user->userInfo()->save(UserInfo::factory()->make());
        });
    }
}
