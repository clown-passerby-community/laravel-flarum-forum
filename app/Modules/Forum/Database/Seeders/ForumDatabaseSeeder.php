<?php

namespace App\Modules\Forum\Database\Seeders;

use Illuminate\Database\Seeder;

class ForumDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call([
            UserTableSeeder::class,
        ]);
    }
}
