<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('web_logs')) return;
        Schema::create('web_logs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('log_id')->comment('日志Id');
            $table->unsignedBigInteger('user_id')->default(0)->comment('会员Id');
            $table->string('created_ip', 20)->default('')->comment('创建时的IP');
            $table->string('browser_type', 300)->default('')->comment('创建时浏览器类型');
            $table->boolean('log_status')->default(1)->comment('状态：1.成功；0.失败');
            $table->string('log_description', 200)->default('')->comment('描述');
            $table->string('this_url', 200)->default('')->comment('当前URL');
            $table->string('request_url', 200)->default('')->comment('请求URL');
            $table->string('log_duration', 200)->default(0)->comment('请求时长（微秒）');
            $table->string('start_request_microtime', 200)->default(0)->comment('开始请求的微秒');
            $table->string('end_request_microtime', 200)->default(0)->comment('请求结束的微秒');
            $table->string('log_action', 200)->default('')->comment('请求方法');
            $table->string('log_method', 20)->default('')->comment('请求类型/请求方式');
            $table->json('request_data')->nullable()->comment('请求参数');
            $table->string('extra_data', 2000)->default('')->comment('附加信息');
            $table->unsignedInteger('created_time')->default(0)->comment('创建时间');
            $table->unsignedInteger('updated_time')->default(0)->comment('更新时间');
            $table->boolean('is_delete')->unsigned()->default(0)->comment('是否删除：1：是；0：否');
            $table->index(['is_delete']);
            $table->index(['user_id']);
            $table->comment('访问日志表');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('web_logs');
    }
};
