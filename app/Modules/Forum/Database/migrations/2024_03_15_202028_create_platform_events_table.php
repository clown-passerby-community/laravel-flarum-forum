<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('platform_events')) return;
        Schema::create('platform_events', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('event_type')->unsigned()->default(100)->comment('事件类型');
            $table->char('date', 10)->default('')->comment('年月日');
            $table->bigInteger('login_user_id')->unsigned()->default(0)->comment('登录会员Id');
            $table->bigInteger('user_id')->unsigned()->default(0)->comment('访问会员Id');
            $table->bigInteger('topic_id')->unsigned()->default(0)->comment('访问话题Id');
            $table->bigInteger('dynamic_id')->unsigned()->default(0)->comment('访问动态Id');
            $table->bigInteger('comment_id')->unsigned()->default(0)->comment('动态的评论Id');
            $table->bigInteger('reply_comment_id')->unsigned()->default(0)->comment('动态的回复评论所属的评论Id|评论回复Id');
            $table->integer('created_time')->unsigned()->default(0)->comment('创建时间');
            $table->integer('updated_time')->unsigned()->default(0)->comment('更新时间');
            $table->string('created_ip', 20)->default('')->comment('创建时的IP');
            $table->string('browser_type', 300)->default('')->comment('创建时浏览器类型');
            $table->boolean('is_delete')->unsigned()->default(0)->comment('是否删除');
            $table->index(['event_type', 'date']);
            $table->index(['user_id']);
            $table->index(['dynamic_id']);
            $table->index(['is_delete']);
            $table->comment('平台事件表');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('platform_events');
    }
};
