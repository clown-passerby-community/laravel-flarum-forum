<?php

namespace App\Modules\Forum\App\Livewire\Dynamic;

use App\Modules\Forum\App\Models\Dynamic;
use Livewire\Component;
use Livewire\WithPagination;

class Lists extends Component
{
    use WithPagination;

    // 可指定模板
    // protected $paginationTheme = 'bootstrap';

    /**
     * @var string
     */
    public $search = '';

    public $topic_id = null;

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var array
     */
    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    /**
     * 初始化.
     */
    public function mount($topic_id = null): void
    {
        // var_dump(request()->all());
        // var_dump($topic_id);
        $this->fill(array_merge(request()->only('search', 'page'), compact('topic_id')));
    }

    public function render()
    {
        $topic_id = $this->topic_id;
        $dynamics = Dynamic::where(function ($query) use ($topic_id){
                if ($topic_id > 0){
                    $query->where('topic_id', $topic_id);
                }
            })
            ->where('dynamic_title', 'LIKE', '%' . trim($this->search) . '%')
            ->orderByDesc('dynamic_id')
            ->paginate(10);
        return view('forum::livewire.dynamic.lists', compact('dynamics'));
    }
}
