<?php

namespace App\Modules\Forum\App\Jobs;

use App\Modules\Forum\App\Models\PlatformEvent;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Arr;

class PlatformEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * 任务执行失败后的重试次数，即最大执行次数为 $maxAttempts+1 次
     *
     * @var int
     */
    protected $maxAttempts = 2;

    protected $event_type;
    protected $params = [];
    protected $visit_time = null;

    /**
     * Create a new job instance.
     */
    public function __construct($event_type, $params = [], $visit_time = null)
    {
        $this->event_type = $event_type;
        $this->params = $params;
        $this->visit_time = $visit_time;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $params = $this->params;
        // 访问时间
        $visit_time = $this->visit_time;
        if (empty($visit_time)){
            $visit_time = strtotime(Carbon::now()->toDateTimeString());
        }

        $platformEvent = new PlatformEvent;
        $platformEvent->event_type = $this->event_type;
        $platformEvent->date = $this->event_type;
        $platformEvent->login_user_id = (int)Arr::get($params, 'login_user_id', 0);
        $platformEvent->user_id = (int)Arr::get($params, 'user_id', 0);
        $platformEvent->topic_id = (int)Arr::get($params, 'topic_id', 0);
        $platformEvent->dynamic_id = (int)Arr::get($params, 'dynamic_id', 0);
        $platformEvent->comment_id = (int)Arr::get($params, 'comment_id', 0);
        $platformEvent->reply_comment_id = (int)Arr::get($params, 'reply_comment_id', 0);
        // 访问时间
        $platformEvent->date = date('Y-m-d', $visit_time);
        $platformEvent->created_time = $visit_time;
        $platformEvent->created_ip = Arr::get($params, 'created_ip', '');
        $platformEvent->browser_type = Arr::get($params, 'browser_type', '');
        $platformEvent->save();
    }
}
