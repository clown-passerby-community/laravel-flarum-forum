<?php

namespace App\Modules\Forum\App\Models;

use App\Models\Model;
use App\Modules\Forum\App\Jobs\PlatformEventJob;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlatformEvent extends Model
{
    use SoftDeletes;

    // 访问事件
    const EVENT_VISIT = 100;
    // [X] 访问动态详情事件
    const EVENT_VISIT_DYNAMIC = 101;
    // [X] 访问话题详情事件
    const EVENT_VISIT_TOPIC = 102;
    // [X] 访问会员详情事件
    const EVENT_VISIT_USER = 103;
    // 发布动态事件
    const EVENT_DYNAMIC_PUBLISH = 201;
    // 点赞动态事件
    const EVENT_DYNAMIC_PRAISE = 202;
    // 收藏动态事件
    const EVENT_DYNAMIC_COLLECTION = 203;
    // 评论动态事件
    const EVENT_DYNAMIC_COMMENT = 204;
    // 删除动态事件
    const EVENT_DYNAMIC_DELETE = 205;

    protected $guarded = [];

    public static function record($event_type, $params, $visit_time = null)
    {
        if (!$visit_time || is_null($visit_time)){
            $visit_time = time();
        }
        PlatformEventJob::dispatchSync($event_type, $params, $visit_time);
    }
}
