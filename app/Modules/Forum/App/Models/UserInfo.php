<?php

namespace App\Modules\Forum\App\Models;

use App\Models\Model;
use App\Modules\Forum\Database\factories\UserInfoFactory;

class UserInfo extends Model
{
    protected $primaryKey = 'user_id';

    protected static function newFactory(): UserInfoFactory
    {
        return UserInfoFactory::new();
    }

    // 会员的基础扩展字段
    const BASIC_EXTENDS_FIELDS = [
        'user_birth' => '', // 生日
        'location' => '', // 所在城市地区
        'user_introduction' => '', // 个人介绍
        'get_likes' => 0, // 获赞数
        'dynamics_count' => 0, // 动态总量
        'fans_count' => 0, // 粉丝数量
        'follows_count' => 0, // 关注数量
        'views_count' => 0, // 访问量：会员主页 + 发布动态详情的访问
    ];

    public function getBasicExtendsAttribute()
    {
        return \array_merge(self::BASIC_EXTENDS_FIELDS, json_decode($this->attributes['basic_extends'] ?? '{}', true));
    }

    public function setBasicExtendsAttribute($value)
    {
        $this->attributes['basic_extends'] = json_encode(array_merge(json_decode($this->attributes['basic_extends'] ?? '{}', true), $value), SON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
