<?php

namespace App\Modules\Forum\App\Models;

use App\Models\Model;

class Dynamic extends Model
{
    // 内容格式类型
    const CONTENT_TYPE_MARKDOWN = 'markdown';
    const CONTENT_TYPE_HTML = 'html';

    protected $primaryKey = 'dynamic_id';
    protected $is_delete  = 0;

    /**
     * 只查询 启用 的作用域
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCheck($query)
    {
        return $query->where('is_check', 1);
    }
}
