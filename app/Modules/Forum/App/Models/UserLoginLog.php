<?php

namespace App\Modules\Forum\App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLoginLog extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'log_id';

    protected $guarded = [];

    public function add(int $user_id, int $log_status = 1, $description = '登录成功')
    {
        $request = request();
        $ip_agent = get_client_info();
        return $this->setMonthTable()->create([
            'user_id' => $user_id,
            'created_ip'   => $request->ip() ?? $ip_agent['ip'] ?? get_ip(),
            'browser_type' => $request->userAgent() ?? $ip_agent['agent'] ?? $_SERVER['HTTP_USER_AGENT'],
            'log_status' => $log_status,
            'description' => $description,
            'log_duration' => microtime(true) - LARAVEL_START,
            'request_data' => my_json_encode(request()->all()),
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
