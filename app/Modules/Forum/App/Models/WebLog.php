<?php

namespace App\Modules\Forum\App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLog extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'log_id';

    protected $guarded = [];

    public function getLogDurationAttribute($key)
    {
        return (float)$key;
    }

    public function setExtraDataAttribute($value)
    {
        if (empty($value)) {
            $value = [];
        }
        if (!is_string($value)) {
            $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        }
        $this->attributes['extra_data'] = $value;
    }

    public function getExtraDataAttribute()
    {
        $value = [];
        if (array_key_exists('extra_data', $this->attributes)) {
            $value = $this->attributes['extra_data'];
            if (empty($value)) {
                return [];
            }
            if (!is_array($value)) {
                $value = json_decode($value, true);
            }
        }
        return $value;
    }
}
