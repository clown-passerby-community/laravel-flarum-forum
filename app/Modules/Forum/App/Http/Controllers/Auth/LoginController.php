<?php

namespace App\Modules\Forum\App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Forum\App\Http\Requests\Auth\LoginRequest;
use App\Modules\Forum\App\Models\UserLoginLog;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        // 登录日志
        UserLoginLog::getInstance()->add(Auth::id());

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
