<?php

namespace App\Modules\Forum\App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Forum\App\Jobs\PlatformEventJob;
use App\Modules\Forum\App\Models\Dynamic;
use App\Modules\Forum\App\Models\PlatformEvent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('forum::index');
    }

    public function show($dynamic_id, Request $request)
    {
        $dynamic = Dynamic::find($dynamic_id);
        // var_dump($dynamic);
        // exit;

        // 记录`访问动态详情`事件
        PlatformEvent::record(PlatformEvent::EVENT_VISIT_DYNAMIC, [
            'login_user_id' => Auth::id(),
            'dynamic_id' => $dynamic->dynamic_id,
            'created_ip'   => $request->ip(),
            'browser_type' => $request->userAgent(),
        ]);

        return view('forum::show', compact('dynamic'));
    }

    public function functions()
    {
        $functions = file_get_contents(base_path() . '/功能列表.md');
        return view('forum::functions', compact('functions'));
    }
}
