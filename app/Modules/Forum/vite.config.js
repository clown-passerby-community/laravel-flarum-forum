import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

/**
 * 解决错误：`__dirname is not defined in ES module scope`
 */
import path from 'path';
import {fileURLToPath} from 'url';
const __filename = fileURLToPath(import.meta.url);
// console.log(__filename)

// 👇️ "/home/borislav/Desktop/javascript"
const __dirname = path.dirname(__filename);
// console.log('directory-name 👉️', __dirname);

export default defineConfig({
    build: {
        outDir: '../../public/build-forum',
        emptyOutDir: true,
        manifest: true,
    },
    plugins: [
        laravel({
            publicDirectory: '../../public',
            buildDirectory: 'build-forum',
            input: [
                __dirname + '/resources/assets/sass/app.scss',
                __dirname + '/resources/assets/js/app.js'
            ],
            refresh: true,
        }),
    ],
});

// // 如果是`npm run build`时，则不可使用`$STUDLY_NAME$`，要么就更换`$STUDLY_NAME$`为当前模块的目录名称！
// const command = process.argv[2];
// if (command != 'build'){
//     export const paths = [
//         'Modules/$STUDLY_NAME$/resources/assets/sass/app.scss',
//         'Modules/$STUDLY_NAME$/resources/assets/js/app.js',
//     ];
// }

export const paths = [
    'app/Modules/Forum/resources/assets/sass/app.scss',
    'app/Modules/Forum/resources/assets/js/app.js',
];
