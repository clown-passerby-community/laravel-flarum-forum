<?php

namespace App\Modules\User\App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Forum\App\Models\PlatformEvent;
use App\Modules\Forum\App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('user::index');
    }

    protected function visitUserProcess($user_id, $request)
    {
        $user_info = UserInfo::findOrFail($user_id);

        // 记录`访问会员详情`事件
        PlatformEvent::record(PlatformEvent::EVENT_VISIT_USER, [
            'login_user_id' => Auth::id(),
            'user_id' => $user_info->user_id,
            'created_ip'   => $request->ip(),
            'browser_type' => $request->userAgent(),
        ]);

        return $user_info;
    }

    /**
     * Show the specified resource.
     */
    public function show($user_id, Request $request)
    {
        $user_info = $this->visitUserProcess($user_id, $request);

        return view('user::show', compact('user_info'));
    }

    /**
     * Show the specified resource.
     */
    public function actions($user_id, $action = 'dynamics', Request $request)
    {
        $user_info = $this->visitUserProcess($user_id, $request);

        return view('user::actions.' . $action, compact('user_info'));
    }
}
