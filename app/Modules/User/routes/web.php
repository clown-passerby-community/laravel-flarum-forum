<?php

use Illuminate\Support\Facades\Route;
use App\Modules\User\App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('')->middleware([])->group(function () {
    Route::resource('user', UserController::class)->only(['index', 'show'])->names('user');
    Route::get('user/{user_id}/{action}', [UserController::class, 'actions'])->name('user.actions');
});
