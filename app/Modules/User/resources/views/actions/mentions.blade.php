@extends('user::layouts.user-master')
@section('title', '首页')

@section('content')
    <nav class="sideNav UserPage-nav">
        <ul class="">
            <li class="item-nav">
                <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount7">
                    <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown">
                        <span class="Button-label">被提及</span>
                        <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                    </button>
                    <ul class="Dropdown-menu dropdown-menu ">
                        <li class="item-posts">
                            <a class="hasIcon" href="/u/test001" active="false">
                                <i aria-hidden="true" class="icon far fa-comment Button-icon"></i>
                                <span class="Button-label">回复 <span class="Button-badge">2</span>
							</span>
                            </a>
                        </li>
                        <li class="item-discussions">
                            <a class="hasIcon" href="/u/test001/discussions" active="false">
                                <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                                <span class="Button-label">主题 <span class="Button-badge">1</span>
							</span>
                            </a>
                        </li>
                        <li class="item-likes">
                            <a class="hasIcon" href="/u/test001/likes" active="false">
                                <i aria-hidden="true" class="icon far fa-thumbs-up Button-icon"></i>
                                <span class="Button-label">赞</span>
                            </a>
                        </li>
                        <li class="item-byobu">
                            <a class="hasIcon" href="/u/test001/private" active="false">
                                <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                <span class="Button-label">私密主题</span>
                            </a>
                        </li>
                        <li class="item-mentions active">
                            <a class="hasIcon" href="/u/test001/mentions" name="mentions" active="true">
                                <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                                <span class="Button-label">被提及</span>
                            </a>
                        </li>
                        <li class="Dropdown-separator"></li>
                        <li class="item-security">
                            <a class="hasIcon" href="/u/test001/security" active="false">
                                <i aria-hidden="true" class="icon fas fa-shield-alt Button-icon"></i>
                                <span class="Button-label">Security</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="item-lastViewedUsers">
                <fieldset class="LastUsers">
                    <legend>足迹</legend>
                    <ul>
                        <li class="item-lastUser-0">
                            <div class="item-lastUser-content">
                                <span class="Avatar" loading="lazy"></span>
                                <div>访客<span class="lastUser-visited" title="2024/3/25 16:56:16">1 分钟前</span>
                                </div>
                            </div>
                        </li>
                        <li class="item-lastUser-1">
                            <a href="http://flarum.local.com:8090/u/Admin">
                                <div class="item-lastUser-content">
                                    <span class="Avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                    <div>Admin<span class="lastUser-visited" title="2024/3/8 15:09:16">17 天前</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </fieldset>
            </li>
        </ul>
    </nav>
    <div class="sideNavOffset UserPage-content">
        <div class="PostsUserPage">
            <ul class="PostsUserPage-list">
                <li>
                    <div class="PostsUserPage-discussion">于 <a href="/d/27-da-sa-da-da-mou-ha-ha/2">大萨达大某哈哈🤣</a>
                    </div>
                    <article class="CommentPost Post Post--by-actor Post--by-start-user">
                        <div>
                            <header class="Post-header">
                                <ul>
                                    <li class="item-user">
                                        <div class="PostUser">
                                            <h3 class="PostUser-name">
                                                <a href="/u/admin">
                                                    <span class="Avatar PostUser-avatar" loading="lazy" style="--avatar-bg: #e5a0cd; border-color: rgb(183, 42, 42);">小</span>
                                                    <span class="UserOnline">
													<i aria-hidden="true" class="icon fas fa-circle"></i>
												</span>
                                                    <span class="username">小丑路人</span>
                                                </a>
                                            </h3>
                                            <ul class="PostUser-badges badges">
                                                <li class="item-group1">
                                                    <div class="Badge Badge--group--1 text-contrast--light" title="" aria-label="管理员" style="--badge-bg: #B72A2A;" data-original-title="管理员">
                                                        <i aria-hidden="true" class="icon fas fa-wrench Badge-icon"></i>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="PostUser-level" title="" aria-label="960 点经验" data-original-title="960 点经验">
											<span class="PostUser-text">
												<span class="PostUser-levelText">Lv.</span>&nbsp;<span class="PostUser-levelPoints">7</span>
											</span>
                                                <div class="PostUser-bar PostUser-bar--empty"></div>
                                                <div class="PostUser-bar" style="width: 11.1111%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-meta">
                                        <div class="Dropdown PostMeta">
                                            <a class="Dropdown-toggle" data-toggle="dropdown">
                                                <time pubdate="" datetime="2024-03-25T16:56:44+08:00" title="2024年3月25日星期一 16点56分" data-humantime="">2 分钟前</time>
                                            </a>
                                            <div class="Dropdown-menu dropdown-menu">
                                                <span class="PostMeta-number">发布 #2</span>
                                                <span class="PostMeta-time">
												<time pubdate="" datetime="2024-03-25T16:56:44+08:00">2024年3月25日星期一 16点56分</time>
											</span>
                                                <span class="PostMeta-ip">
												<div class="ip-container">
													<div class="ip-info">
														<span title="" aria-label="private range " data-original-title="private range ">172.19.0.1</span>
													</div>
												</div>
											</span>
                                                <input class="FormControl PostMeta-permalink">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </header>
                            <div class="Post-body">
                                <p>
                                    <a href="http://flarum.local.com:8090/u/test001" class="UserMention" rel="" target="">@测试1</a> 哈哈
                                </p>
                            </div>
                            <aside class="Post-actions">
                                <ul>
                                    <li class="item-react">
                                        <div class="Reactions" style="margin-right: 7px;">
                                            <div class="Reactions--reactions"></div>
                                            <div class="Reactions--react">
                                                <button class="Button Button--link Reactions--ShowReactions" type="Button" aria-label="戳表情">
												<span class="Button-label">
													<span class="Button-label">
														<svg width="20px" height="20px" viewBox="0 0 18 18" class="button-react">
															<g id="Reaction" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<g id="ic_reactions_grey">
																	<g id="Group-2">
																		<g id="0:0:0:0">
																			<rect id="Rectangle-5" x="0" y="0" width="18" height="18"></rect>
																			<g id="emoticon"></g>
																			<path d="M14.6332705,7.33333333 C14.6554304,7.55389388 14.6666667,7.77636769 14.6666667,8 C14.6666667,11.6818983 11.6818983,14.6666667 8,14.6666667 C6.23189007,14.6666667 4.53619732,13.9642877 3.28595479,12.7140452 C2.03571227,11.4638027 1.33333333,9.76810993 1.33333333,8 C1.33333333,4.33333333 4.31333333,1.33333333 8,1.33333333 L8,1.33333333 C8.22363231,1.33333333 8.44610612,1.3445696 8.66666667,1.36672949 L8.66666667,2.70847693 C8.44668912,2.68076722 8.22407146,2.66666667 8,2.66666667 C5.05448133,2.66666667 2.66666667,5.05448133 2.66666667,8 C2.66666667,10.9455187 5.05448133,13.3333333 8,13.3333333 C10.9455187,13.3333333 13.3333333,10.9455187 13.3333333,8 C13.3333333,7.77592854 13.3192328,7.55331088 13.2915231,7.33333333 L14.6332705,7.33333333 Z M8,11.6666667 C9.55333333,11.6666667 10.8666667,10.6933333 11.4066667,9.33333333 L4.59333333,9.33333333 C5.12666667,10.6933333 6.44666667,11.6666667 8,11.6666667 Z M10.3333333,7.33333333 C10.8856181,7.33333333 11.3333333,6.88561808 11.3333333,6.33333333 C11.3333333,5.78104858 10.8856181,5.33333333 10.3333333,5.33333333 C9.78104858,5.33333333 9.33333333,5.78104858 9.33333333,6.33333333 C9.33333333,6.88561808 9.78104858,7.33333333 10.3333333,7.33333333 L10.3333333,7.33333333 Z M5.66666667,7.33333333 C6.21895142,7.33333333 6.66666667,6.88561808 6.66666667,6.33333333 C6.66666667,5.78104858 6.21895142,5.33333333 5.66666667,5.33333333 C5.11438192,5.33333333 4.66666667,5.78104858 4.66666667,6.33333333 C4.66666667,6.88561808 5.11438192,7.33333333 5.66666667,7.33333333 Z" id="Combined-Shape" fill="#667c99"></path>
																		</g>
																		<g id="Group-15" transform="translate(10.666667, 0.000000)" fill="#667c99">
																			<polygon id="Path" points="3.33333333 2 3.33333333 0 2 0 2 2 0 2 0 3.33333333 2 3.33333333 2 5.33333333 3.33333333 5.33333333 3.33333333 3.33333333 5.33333333 3.33333333 5.33333333 2"></polygon>
																		</g>
																	</g>
																</g>
															</g>
														</svg>
													</span>
												</span>
                                                </button>
                                                <div class="CommentPost--Reactions" style="left: -28%;">
                                                    <ul class="Reactions--Ul">
                                                        <li class="item-thumbsup">
                                                            <button class="Button Button--link" type="button" aria-label="thumbsup" data-reaction="thumbsup">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44d.png" loading="lazy" draggable="true" alt="thumbsup" title="thumbsup">
															</span>
                                                            </button>
                                                        </li>
                                                        <li class="item-thumbsdown">
                                                            <button class="Button Button--link" type="button" aria-label="thumbsdown" data-reaction="thumbsdown">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f44e.png" loading="lazy" draggable="true" alt="thumbsdown" title="thumbsdown">
															</span>
                                                            </button>
                                                        </li>
                                                        <li class="item-laughing">
                                                            <button class="Button Button--link" type="button" aria-label="laughing" data-reaction="laughing">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f606.png" loading="lazy" draggable="true" alt="laughing" title="laughing">
															</span>
                                                            </button>
                                                        </li>
                                                        <li class="item-confused">
                                                            <button class="Button Button--link" type="button" aria-label="confused" data-reaction="confused">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f615.png" loading="lazy" draggable="true" alt="confused" title="confused">
															</span>
                                                            </button>
                                                        </li>
                                                        <li class="item-heart">
                                                            <button class="Button Button--link" type="button" aria-label="heart" data-reaction="heart">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/2764.png" loading="lazy" draggable="true" alt="heart" title="heart">
															</span>
                                                            </button>
                                                        </li>
                                                        <li class="item-tada">
                                                            <button class="Button Button--link" type="button" aria-label="tada" data-reaction="tada">
															<span class="Button-label">
																<img class="emoji" src="https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/1f389.png" loading="lazy" draggable="true" alt="tada" title="tada">
															</span>
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-reply">
                                        <button class="Button Button--link" type="button">
                                            <span class="Button-label">回复</span>
                                        </button>
                                    </li>
                                    <li class="item-like">
                                        <button class="Button Button--link" type="button">
                                            <span class="Button-label">赞</span>
                                        </button>
                                    </li>
                                    <li>
                                        <div class="ButtonGroup Dropdown dropdown Post-controls itemCount5">
                                            <button class="Dropdown-toggle Button Button--icon Button--flat" aria-haspopup="menu" aria-label="帖子下拉菜单开关" data-toggle="dropdown">
                                                <i aria-hidden="true" class="icon fas fa-ellipsis-h Button-icon"></i>
                                                <span class="Button-label"></span>
                                                <i aria-hidden="true" class="icon fas fa-caret-down Button-caret"></i>
                                            </button>
                                            <ul class="Dropdown-menu dropdown-menu Dropdown-menu--right">
                                                <li class="item-edit">
                                                    <button class="hasIcon" type="button">
                                                        <i aria-hidden="true" class="icon fas fa-pencil-alt Button-icon"></i>
                                                        <span class="Button-label">编辑</span>
                                                    </button>
                                                </li>
                                                <li class="item-viewReactions">
                                                    <button class="hasIcon" type="button">
                                                        <i aria-hidden="true" class="icon fas fa-heart Button-icon"></i>
                                                        <span class="Button-label">查看表情</span>
                                                    </button>
                                                </li>
                                                <li class="item-addPoll">
                                                    <button class="hasIcon" type="button">
                                                        <i aria-hidden="true" class="icon fas fa-poll Button-icon"></i>
                                                        <span class="Button-label">Add Poll</span>
                                                    </button>
                                                </li>
                                                <li class="Dropdown-separator"></li>
                                                <li class="item-hide">
                                                    <button class="hasIcon" type="button">
                                                        <i aria-hidden="true" class="icon far fa-trash-alt Button-icon"></i>
                                                        <span class="Button-label">删除</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </aside>
                            <footer class="Post-footer"></footer>
                        </div>
                    </article>
                    <div class="Post-quoteButtonContainer"></div>
                </li>
            </ul>
            <div class="PostsUserPage-loadMore"></div>
        </div>
    </div>
@endsection


