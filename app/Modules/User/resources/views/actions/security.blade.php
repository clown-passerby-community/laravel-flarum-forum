@extends('user::layouts.user-master')
@section('title', '首页')

@section('content')
    <nav class="sideNav UserPage-nav">
        <ul class="">
            <li class="item-nav">
                <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount7">
                    <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown">
                        <span class="Button-label">Security</span>
                        <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                    </button>
                    <ul class="Dropdown-menu dropdown-menu ">
                        <li class="item-posts">
                            <a class="hasIcon" href="/u/test001" active="false">
                                <i aria-hidden="true" class="icon far fa-comment Button-icon"></i>
                                <span class="Button-label">回复 <span class="Button-badge">2</span>
							</span>
                            </a>
                        </li>
                        <li class="item-discussions">
                            <a class="hasIcon" href="/u/test001/discussions" active="false">
                                <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                                <span class="Button-label">主题 <span class="Button-badge">1</span>
							</span>
                            </a>
                        </li>
                        <li class="item-likes">
                            <a class="hasIcon" href="/u/test001/likes" active="false">
                                <i aria-hidden="true" class="icon far fa-thumbs-up Button-icon"></i>
                                <span class="Button-label">赞</span>
                            </a>
                        </li>
                        <li class="item-byobu">
                            <a class="hasIcon" href="/u/test001/private" active="false">
                                <i aria-hidden="true" class="icon fas fa-map Button-icon"></i>
                                <span class="Button-label">私密主题</span>
                            </a>
                        </li>
                        <li class="item-mentions">
                            <a class="hasIcon" href="/u/test001/mentions" name="mentions" active="false">
                                <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                                <span class="Button-label">被提及</span>
                            </a>
                        </li>
                        <li class="Dropdown-separator"></li>
                        <li class="item-security active">
                            <a class="hasIcon" href="/u/test001/security" active="true">
                                <i aria-hidden="true" class="icon fas fa-shield-alt Button-icon"></i>
                                <span class="Button-label">Security</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="item-lastViewedUsers">
                <fieldset class="LastUsers">
                    <legend>足迹</legend>
                    <ul>
                        <li class="item-lastUser-0">
                            <div class="item-lastUser-content">
                                <span class="Avatar" loading="lazy"></span>
                                <div>访客<span class="lastUser-visited" title="2024/3/28 11:00:34">几秒前</span>
                                </div>
                            </div>
                        </li>
                        <li class="item-lastUser-1">
                            <a href="http://flarum.local.com:8090/u/Admin">
                                <div class="item-lastUser-content">
                                    <span class="Avatar" loading="lazy" style="--avatar-bg: #e5a0cd;">小</span>
                                    <div>Admin<span class="lastUser-visited" title="2024/3/25 17:00:51">3 天前</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </fieldset>
            </li>
        </ul>
    </nav>
    <div class="sideNavOffset UserPage-content">
        <div class="UserSecurityPage">
            <ul>
                <li class="item-linkedAccounts">
                    <fieldset>
                        <legend>第三方登录</legend>
                        <ul>
                            <li class="">
                                <p class="helpText">绑定第三方帐户，开辟登录捷径。</p>
                            </li>
                            <li class="">
                                <ul class="LinkedAccounts-List">
                                    <li class="item-discord">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--discord">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-discord Provider-Icon Provider-Icon--discord"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">Discord</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-facebook">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--facebook">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-facebook Provider-Icon Provider-Icon--facebook"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">Facebook</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-github">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--github">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-github Provider-Icon Provider-Icon--github"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">GitHub</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-gitlab">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--gitlab">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-gitlab Provider-Icon Provider-Icon--gitlab"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">GitLab</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-twitter">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--twitter">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-twitter Provider-Icon Provider-Icon--twitter"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">Twitter</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-google">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--google">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-google Provider-Icon Provider-Icon--google"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">Google</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-linkedin">
                                        <div class="LinkedAccounts-Account LinkedAccounts-Account--linkedin">
                                            <div class="LinkedAccountsList-item-icon">
                                                <i aria-hidden="true" class="icon fab fa-linkedin Provider-Icon Provider-Icon--linkedin"></i>
                                            </div>
                                            <div>
                                                <p class="LinkedAccountsList-item-title">领英</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </fieldset>
                </li>
                <li class="item-developerTokens">
                    <fieldset class="UserSecurityPage-developerTokens">
                        <legend>Developer Tokens</legend>
                        <ul>
                            <li class="item-accessTokenList">
                                <div class="AccessTokensList">
                                    <div class="AccessTokensList--empty">It looks like there is nothing to see here.</div>
                                </div>
                            </li>
                        </ul>
                    </fieldset>
                </li>
                <li class="item-sessions">
                    <fieldset class="UserSecurityPage-sessions">
                        <legend>Active Sessions</legend>
                        <ul>
                            <li class="item-sessionsList">
                                <div class="AccessTokensList">
                                    <div class="AccessTokensList--empty">It looks like there is nothing to see here.</div>
                                </div>
                            </li>
                        </ul>
                    </fieldset>
                </li>
            </ul>
        </div>
    </div>
@endsection


