@extends('user::layouts.user-master')
@section('title', '首页')

@section('content')
    <nav class="sideNav UserPage-nav">
        <ul class="">
            <li class="item-nav">
                <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount4">
                    <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown">
                        <span class="Button-label">主题</span>
                        <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                    </button>
                    <ul class="Dropdown-menu dropdown-menu ">
                        <li class="item-posts">
                            <a class="hasIcon" href="/u/admin" active="false">
                                <i aria-hidden="true" class="icon far fa-comment Button-icon"></i>
                                <span class="Button-label">回复 <span class="Button-badge">31</span>
										</span>
                            </a>
                        </li>
                        <li class="item-discussions active">
                            <a class="hasIcon" href="/u/admin/discussions" active="true">
                                <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                                <span class="Button-label">主题 <span class="Button-badge">24</span>
										</span>
                            </a>
                        </li>
                        <li class="item-likes">
                            <a class="hasIcon" href="/u/admin/likes" active="false">
                                <i aria-hidden="true" class="icon far fa-thumbs-up Button-icon"></i>
                                <span class="Button-label">赞</span>
                            </a>
                        </li>
                        <li class="item-mentions">
                            <a class="hasIcon" href="/u/admin/mentions" name="mentions" active="false">
                                <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                                <span class="Button-label">被提及</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="item-lastViewedUsers">
                <fieldset class="LastUsers">
                    <legend>足迹</legend>
                    <ul></ul>
                </fieldset>
            </li>
        </ul>
    </nav>
    <div class="sideNavOffset UserPage-content">
        <div class="DiscussionsUserPage">
            <div class="DiscussionList">
                <ul role="feed" class="DiscussionList-discussions">
                    <li data-id="3" role="article" aria-setsize="-1" aria-posinset="0">
                        <div class="DiscussionListItem DiscussionListItem--sticky">
					<span class="Slidable-underneath Slidable-underneath--left Slidable-underneath--elastic disabled">
						<i aria-hidden="true" class="icon fas fa-check"></i>
					</span>
                            <div class="DiscussionListItem-content Slidable-content">
                                <a class="DiscussionListItem-author" href="/u/test001" title="" aria-label="测试1 发布于 18 1月" data-original-title="测试1 发布于 18 1月">
                                    <span class="Avatar" loading="lazy" style="--avatar-bg: #b1e5a0;">测</span>
                                </a>
                                <ul class="DiscussionListItem-badges badges">
                                    <li class="item-sticky">
                                        <div class="Badge Badge--sticky text-contrast--unchanged" title="" aria-label="置顶" style="" data-original-title="置顶">
                                            <i aria-hidden="true" class="icon fas fa-thumbtack Badge-icon"></i>
                                        </div>
                                    </li>
                                </ul>
                                <a href="/d/3-002a" class="DiscussionListItem-main">
                                    <h2 class="DiscussionListItem-title">002啊</h2>
                                    <ul class="DiscussionListItem-info">
                                        <li class="item-tags">
									<span class="TagsLabel">
										<span class="TagLabel " style="">
											<span class="TagLabel-text">
												<span class="TagLabel-name">技术文章</span>
											</span>
										</span>
									</span>
                                        </li>
                                        <li class="item-terminalPost">
									<span>
										<span class="username">测试1</span> 发布于 <time pubdate="" datetime="2024-01-18T11:20:00+08:00" title="2024年1月18日星期四 11点20分" data-humantime="">18 1月</time>
									</span>
                                        </li>
                                    </ul>
                                </a>
                                <span class="DiscussionListItem-count">
							<span aria-hidden="true">1</span>
							<span class="visually-hidden">1 条回复</span>
						</span>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="DiscussionList-loadMore"></div>
            </div>
        </div>
    </div>
@endsection


