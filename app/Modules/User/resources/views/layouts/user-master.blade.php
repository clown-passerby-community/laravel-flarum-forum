<x-app-layout>
    <div class="UserPage">
        <div class="UserCard Hero UserHero" style="--usercard-bg: #e5a0cd;">
            <div class="darkenBackground">
                <div class="container">
                    <div class="UserCard-profile">
                        <h1 class="UserCard-identity">
                            <a href="/u/admin">
                                <div class="UserCard-avatar">
                                    <span loading="eager" class="Avatar" style="--avatar-bg: #e5a0cd; border-color: rgb(183, 42, 42);">小</span>
                                </div>
                                <span class="username">{{ $user_info->nick_name }}</span>
                            </a>
                        </h1>
                        <ul class="UserCard-badges badges">
                            <li class="item-group1">
                                <div class="Badge Badge--group--1 text-contrast--light" title="" aria-label="管理员" style="--badge-bg: #B72A2A;" data-original-title="管理员">
                                    <i aria-hidden="true" class="icon fas fa-wrench Badge-icon"></i>
                                </div>
                            </li>
                        </ul>
                        <ul class="UserCard-info">
                            <li class="item-lastSeen">
                                <span class="UserCard-lastSeen" title="{{ date('Y-m-d H:i', $user_info->last_actived_time) }}">
                                    <i aria-hidden="true" class="icon far fa-clock"></i> {{ formatting_timestamp($user_info->last_actived_time) }}
                                </span>
                            </li>
                            <li class="item-joined" title="{{ date('Y-m-d H:i', $user_info->created_time) }}">注册于 {{ formatting_timestamp($user_info->created_time) }}</li>
                            <li class="item-best-answer-count">
							<span class="UserCard-bestAnswerCount">
								<i aria-hidden="true" class="icon fas fa-check"></i>0 次助人
							</span>
                            </li>
                            <li class="item-profile-views">
                                <span>
                                    <i aria-hidden="true" class="icon far fa-eye"></i> 访问量 {{ $user_info->basic_extends['views_count'] ?? 0 }}
                                </span>
                            </li>
                            <li class="item-profile-likes">
                                <span>
                                    <i aria-hidden="true" class="icon far fa-like"></i> 点赞量 {{ $user_info->basic_extends['get_likes'] ?? 0 }}
                                </span>
                            </li>
                            <li class="item-money">
                                <span>🆚</span>
                            </li>
                            @if($user_info->basic_extends && isset($user_info->basic_extends['user_introduction']) && !empty($user_info->basic_extends['user_introduction']))
                                <li class="item-bio">
                                    <div class="UserBio ">
                                        <div class="UserBio-content" style="--bio-max-lines: 0;">
                                            <p>{{ $user_info->basic_extends['user_introduction'] }}</p>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="sideNavContainer">
                @yield('content')
            </div>
        </div>
    </div>
</x-app-layout>
