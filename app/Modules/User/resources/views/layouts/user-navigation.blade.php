<nav class="sideNav UserPage-nav">
    <ul class="">
        <li class="item-nav">
            <div class="ButtonGroup Dropdown dropdown App-titleControl Dropdown--select itemCount4">
                <button class="Dropdown-toggle Button" aria-haspopup="menu" aria-label="下拉菜单开关" data-toggle="dropdown">
                    <span class="Button-label">主题</span>
                    <i aria-hidden="true" class="icon fas fa-sort Button-caret"></i>
                </button>
                <ul class="Dropdown-menu dropdown-menu ">
                    <li class="item-posts">
                        <a class="hasIcon" href="/u/admin" active="false">
                            <i aria-hidden="true" class="icon far fa-comment Button-icon"></i>
                            <span class="Button-label">回复 <span class="Button-badge">31</span>
										</span>
                        </a>
                    </li>
                    <li class="item-discussions active">
                        <a class="hasIcon" href="/u/admin/discussions" active="true">
                            <i aria-hidden="true" class="icon fas fa-bars Button-icon"></i>
                            <span class="Button-label">主题 <span class="Button-badge">24</span>
										</span>
                        </a>
                    </li>
                    <li class="item-likes">
                        <a class="hasIcon" href="/u/admin/likes" active="false">
                            <i aria-hidden="true" class="icon far fa-thumbs-up Button-icon"></i>
                            <span class="Button-label">赞</span>
                        </a>
                    </li>
                    <li class="item-mentions">
                        <a class="hasIcon" href="/u/admin/mentions" name="mentions" active="false">
                            <i aria-hidden="true" class="icon fas fa-at Button-icon"></i>
                            <span class="Button-label">被提及</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="item-lastViewedUsers">
            <fieldset class="LastUsers">
                <legend>足迹</legend>
                <ul></ul>
            </fieldset>
        </li>
    </ul>
</nav>
