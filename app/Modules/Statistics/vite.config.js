import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

/**
 * 解决错误：`__dirname is not defined in ES module scope`
 */
import path from 'path';
import {fileURLToPath} from 'url';
const __filename = fileURLToPath(import.meta.url);
// console.log(__filename)

// 👇️ "/home/borislav/Desktop/javascript"
const __dirname = path.dirname(__filename);
// console.log('directory-name 👉️', __dirname);

export default defineConfig({
    build: {
        outDir: '../../public/build-statistics',
        emptyOutDir: true,
        manifest: true,
    },
    plugins: [
        laravel({
            publicDirectory: '../../public',
            buildDirectory: 'build-statistics',
            input: [
                __dirname + '/resources/assets/sass/app.scss',
                __dirname + '/resources/assets/js/app.js'
            ],
            refresh: true,
        }),
    ],
});

//export const paths = [
//    'Modules/$STUDLY_NAME$/resources/assets/sass/app.scss',
//    'Modules/$STUDLY_NAME$/resources/assets/js/app.js',
//];
export const paths = [
   'Modules/Statistics/resources/assets/sass/app.scss',
   'Modules/Statistics/resources/assets/js/app.js',
];
