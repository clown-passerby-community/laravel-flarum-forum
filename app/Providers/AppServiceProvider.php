<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // 分页使用bootstrap
        // Paginator::useBootstrap();

        // 忽略由 Passport 注册的路由
        Passport::ignoreRoutes();
        /**
         * Token 生命周期
         */
        // 默认令牌发放的有效期是永久
        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        Passport::personalAccessTokensExpireIn(now()->addMonths(6));
    }
}
