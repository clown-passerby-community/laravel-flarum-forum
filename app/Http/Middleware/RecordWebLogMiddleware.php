<?php

namespace App\Http\Middleware;

use App\Modules\Forum\App\Jobs\AsyncWebRecordLogJob;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class RecordWebLogMiddleware
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next)
    {
        // return $next($request);
        $route = $request->route();

        $ip_agent = get_client_info();

        // $user_id = $request->attributes->get('login_user')->user_id ?? 0;
        $user_id = (int)Auth::id();

        $time = time();
        [$t1, $t2] = explode(' ', microtime());
        $end_request_microtime = sprintf('%.10f', (float)bcadd($t1, $t2, 10));
        $web_log_data = [
            'user_id'   => $user_id,
            'request_data' => my_json_encode($request->all()),
            'created_ip'   => $request->ip() ?? $ip_agent['ip'] ?? get_ip(),
            'browser_type' => $request->userAgent() ?? $ip_agent['agent'] ?? $_SERVER['HTTP_USER_AGENT'],
            'created_time' => $time,
            'updated_time' => $time,
            'log_action'   => $route->getActionName(),
            'log_method'   => $request->getMethod(),
            'start_request_microtime' => LARAVEL_START,
            'end_request_microtime' => $end_request_microtime,
            'log_duration' => bcsub($end_request_microtime, LARAVEL_START, 10),
            'request_url'  => URL::previous() ?? get_request_url(),
            'this_url'     => URL::full() ?? get_this_url(),
            // 默认值
            'log_status'   => 0,
            'log_description'   => '异常中断',
            'extra_data' => [
                'route' => [
                    'url' => $route->uri(),
                    'name' => $route->getName(),
                    'prefix' => $route->getPrefix(),
                    'action' => $route->getAction(),
                    'methods' => $route->methods(),
                    'action_name' => $route->getActionName(),
                    'domain' => $route->getDomain(),
                ]
            ]
        ];

        $log_status = 0;
        $log_description = $web_log_data['log_description'];
        try{
            $response = $next($request);
            if ($response instanceof Response || method_exists($response, 'getContent')){ // Web
                $log_status = $response->getStatusCode() == 200 ? 1 : 0;
                // var_dump($response->getContent());
                $pattern = '/<title>(.*?)<\/title>/'; // 匹配 <title>...</title> 标签及其内容的正则表达式;
                if (preg_match($pattern, $response->getContent(), $matches)){
                    $log_description = $matches[1] ?? '';
                }
            } else if (method_exists($response, 'getData')){ // API
                $response_body_content = $response->getData() ?? [];
                // 根据接口响应，存储返回状态与文本提示语
                $log_status = $response_body_content->status == 1 ? 1 : 0;
                $log_description = $response_body_content->msg;
            }else{
                $log_description = 'Method Illuminate\Http\Response::getData does not exist.';
            }
        }catch(\Exception $e){
            Log::debug($log_description);
            $log_description = $e->getMessage();
            $response = $this->errorJson($log_description);
        }

        // 同步更新响应状态与文本，在`handler`层可能会被异常终止
        $web_log_data['log_duration'] = microtime(true) - LARAVEL_START;
        // 根据接口响应，存储返回状态与文本提示语
        $web_log_data['log_status'] = $log_status;
        $web_log_data['log_description'] = $log_description;

        AsyncWebRecordLogJob::dispatchSync($web_log_data);

        return $next($request);
    }
}
