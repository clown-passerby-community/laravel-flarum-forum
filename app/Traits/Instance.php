<?php

declare(strict_types = 1);

namespace App\Traits;

/**
 * Trait Instance
 *
 * 单例
 *
 * @package App\Traits
 */
trait Instance
{
    /**
     * 单例
     */
    protected static $instance;

    // 被继承方如果重写了此方法，访问级别需保持一致！
    // // 私有化构造函数，防止外部直接实例化
    // private function __construct()
    // {
    //     // 初始化操作
    // }

    static function getInstance(...$args)
    {
        if (empty(self::$instance)) {
            self::$instance = new static(...$args);
        }else{
            if(self::$instance instanceof static){

            }else{
                self::$instance = new static(...$args);
            }
        }
        return self::$instance;
    }

    // 防止通过复制实例来创建新实例
    private function __clone()
    {
        // 禁止克隆
    }

    // 被继承方如果重写了此方法，访问级别需保持一致！
    // // 防止通过反序列化来创建新实例
    // private function __wakeup()
    // {
    //     // 禁止反序列化
    // }
}
